webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-sidebar *ngIf=\"routename!='/login' && routename!='/signup'\"></app-sidebar>\r\n  <div class=\"main_wrapper\">\r\n              <div id=\"content\">\r\n<app-main-nav *ngIf=\"routename!='/login' && routename!='/signup'\"></app-main-nav>\r\n  \r\n\r\n<router-outlet></router-outlet>\r\n<app-signup *ngIf=\"routename!='/login' && routename!='/signup'\"></app-signup>\r\n<app-login *ngIf=\"routename!='/login' && routename!='/signup'\"></app-login>\r\n<app-footer *ngIf=\"routename!='/login' && routename!='/signup'\"></app-footer>\r\n\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(location) {
        this.location = location;
        this.title = 'app';
        var pathString = location.path();
        this.routename = pathString;
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common__["f" /* Location */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_carouselamos__ = __webpack_require__("./node_modules/ng2-carouselamos/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__home_home_module__ = __webpack_require__("./src/app/home/home.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__product_category_category_module__ = __webpack_require__("./src/app/product/category/category.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__search_results_search_results_module__ = __webpack_require__("./src/app/search-results/search-results.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__cart_cart_module__ = __webpack_require__("./src/app/cart/cart.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__checkout_checkout_module__ = __webpack_require__("./src/app/checkout/checkout.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__order_confirmation_order_confirmation_module__ = __webpack_require__("./src/app/order-confirmation/order-confirmation.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__product_productpage_productpage_module__ = __webpack_require__("./src/app/product/productpage/productpage.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__profile_profile_module__ = __webpack_require__("./src/app/profile/profile.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__profile_address_book_address_book_module__ = __webpack_require__("./src/app/profile/address-book/address-book.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__profile_track_order_track_order_module__ = __webpack_require__("./src/app/profile/track-order/track-order.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__profile_update_contacts_update_contacts_module__ = __webpack_require__("./src/app/profile/update-contacts/update-contacts.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__shared_layout_sidebar_sidebar_component__ = __webpack_require__("./src/app/shared/layout/sidebar/sidebar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__shared_layout_footer_footer_component__ = __webpack_require__("./src/app/shared/layout/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__shared_layout_login_login_component__ = __webpack_require__("./src/app/shared/layout/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__shared_layout_signup_signup_component__ = __webpack_require__("./src/app/shared/layout/signup/signup.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__loginpage_loginpage_component__ = __webpack_require__("./src/app/loginpage/loginpage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__signuppage_signuppage_component__ = __webpack_require__("./src/app/signuppage/signuppage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__signuppage_signup_service__ = __webpack_require__("./src/app/signuppage/signup.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__loginpage_auth_service__ = __webpack_require__("./src/app/loginpage/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__profile_address_book_address_service_address_service__ = __webpack_require__("./src/app/profile/address-book/address-service/address-service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__home_Banner_banner_service__ = __webpack_require__("./src/app/home/Banner/banner.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__services_Serv_service__ = __webpack_require__("./src/app/services/Serv.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__shared_layout_main_nav_searchsuggestions_service__ = __webpack_require__("./src/app/shared/layout/main-nav/searchsuggestions.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__product_productpage_productpage_service__ = __webpack_require__("./src/app/product/productpage/productpage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__shared_layout_main_nav_main_nav_component__ = __webpack_require__("./src/app/shared/layout/main-nav/main-nav.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__shared__ = __webpack_require__("./src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

































var rootRouting = __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* RouterModule */].forRoot([{ path: 'login', component: __WEBPACK_IMPORTED_MODULE_22__loginpage_loginpage_component__["a" /* LoginpageComponent */] }, { path: 'signup', component: __WEBPACK_IMPORTED_MODULE_23__signuppage_signuppage_component__["a" /* SignuppageComponent */] }
]);
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_18__shared_layout_sidebar_sidebar_component__["a" /* SidebarComponent */],
                __WEBPACK_IMPORTED_MODULE_19__shared_layout_footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_31__shared_layout_main_nav_main_nav_component__["a" /* MainNavComponent */],
                __WEBPACK_IMPORTED_MODULE_20__shared_layout_login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_21__shared_layout_signup_signup_component__["a" /* SignupComponent */],
                __WEBPACK_IMPORTED_MODULE_22__loginpage_loginpage_component__["a" /* LoginpageComponent */],
                __WEBPACK_IMPORTED_MODULE_23__signuppage_signuppage_component__["a" /* SignuppageComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3_ng2_carouselamos__["a" /* Ng2CarouselamosModule */],
                __WEBPACK_IMPORTED_MODULE_32__shared__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_7__home_home_module__["a" /* HomeModule */],
                __WEBPACK_IMPORTED_MODULE_8__product_category_category_module__["a" /* CategoryModule */],
                __WEBPACK_IMPORTED_MODULE_9__search_results_search_results_module__["a" /* SearchResultsModule */],
                __WEBPACK_IMPORTED_MODULE_10__cart_cart_module__["a" /* CartModule */],
                __WEBPACK_IMPORTED_MODULE_11__checkout_checkout_module__["a" /* CheckoutModule */],
                __WEBPACK_IMPORTED_MODULE_12__order_confirmation_order_confirmation_module__["a" /* OrderConfirmationModule */],
                __WEBPACK_IMPORTED_MODULE_13__product_productpage_productpage_module__["a" /* ProductpageModule */],
                rootRouting,
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_14__profile_profile_module__["a" /* ProfileModule */],
                __WEBPACK_IMPORTED_MODULE_15__profile_address_book_address_book_module__["a" /* AddressBookModule */],
                __WEBPACK_IMPORTED_MODULE_16__profile_track_order_track_order_module__["a" /* TrackOrderModule */],
                __WEBPACK_IMPORTED_MODULE_17__profile_update_contacts_update_contacts_module__["a" /* UpdateContactModule */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_24__signuppage_signup_service__["a" /* SignupService */],
                __WEBPACK_IMPORTED_MODULE_27__home_Banner_banner_service__["a" /* BannerService */],
                __WEBPACK_IMPORTED_MODULE_28__services_Serv_service__["a" /* CategoryService */],
                __WEBPACK_IMPORTED_MODULE_25__loginpage_auth_service__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_26__profile_address_book_address_service_address_service__["a" /* AddressService */],
                __WEBPACK_IMPORTED_MODULE_29__shared_layout_main_nav_searchsuggestions_service__["a" /* SearchSuggestionsService */],
                __WEBPACK_IMPORTED_MODULE_30__product_productpage_productpage_service__["a" /* ProductpageService */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/cart/cart.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/cart/cart.component.html":
/***/ (function(module, exports) {

module.exports = "\n<!-- ===========================================\n      Cart Content\n -->\n   <div id=\"content_wrapper\">\n    <br>\n      <div class=\"ct-page\">\n        <!--  -->\n        <div class=\"container-fluid\">\n          <div class=\"row\">\n            <div class=\"col\">\n             <section class=\"text-center\">\n                <h5 class=\"cart-title\">Your shopping Bag</h5>\n                <p>Review of <b id=\"pen\">3</b> items <b id=\"pen\">N139,500</b></p>\n                <hr width=\"20%\">\n             </section>\n            </div>\n          </div>\n        </div>\n        <!-- ************ -->\n        <div class=\"container-fluid\" id=\"cart-feature\">\n          <div class=\"row\">\n            <div class=\"col-md-4 \">\n              <img src=\"assets/webfonts/locked.svg\" width=\"20px\">\n              <div id=\"cart-featured\">\n                <h6><b>SECURITY</b></h6>\n                <p>We use the best security technology to encrypt all personal information</p>\n              </div>\n            </div>\n            <div class=\"col-md-4 \">\n              <img src=\"assets/webfonts/credit-card.svg\" width=\"20px\">\n              <div id=\"cart-featured\">\n                <h6><b>PAYMENT OPTIONS</b></h6>\n                <p>We accept all major credit and debit cards: Visa, Mastercard, American Expresss, Maestro</p>\n              </div>\n            </div>\n            <div class=\"col-md-4 \">\n              <img src=\"assets/webfonts/return-box.svg\" width=\"20px\">\n              <div id=\"cart-featured\">\n                <h6><b>365 DAYS RETURNS</b></h6>\n                <p>If you''re not totally satisfied you can return AMALINZE purchase with 365 days for an exchange or refund\n                </p>\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <!-- *********** -->\n        <div class=\"container-fluid\" id=\"cart_item\">\n          <div class=\"row\">\n\n            <div class=\"col-lg-3\" >\n              <div id=\"cart_product_img\" class=\"text-center\">\n                <img src=\"assets/images/demo2.jpg\" class=\"img-fluid\">\n              </div>   \n            </div>\n\n            <div class=\"col-lg-7 cart_sub\">\n              <h1 class=\"cart_product_title\">Print ethno tank</h1>\n              <p id=\"cart_item_id\">#12343434380930</p>\n              <section id=\"cart_item_spec\">\n                <p><img src=\"assets/webfonts/raindrop-close-up.svg\" width=\"20px\"><span>Black mix</span></p>\n                <p><img src=\"assets/webfonts/shirt.svg\" width=\"20px\"><span>Small Regular</span></p>\n                <p><img src=\"assets/webfonts/shopping-cart.svg\" width=\"20px\"><span>2 Item(s)</span></p>\n              </section>\n            </div>\n\n            <div class=\"col-lg-2 cart_sub\">\n              <div id=\"cart_item_price\">\n                <h2 >N200,000</h2>\n                <p><img src=\"assets/webfonts/cross-out.svg\" width=\"20px\"></p>\n              </div>\n              \n            </div>\n          </div> \n        </div>\n\n        <!-- *********** -->\n        <div class=\"container-fluid\" id=\"cart_item\">\n          <div class=\"row\">\n\n            <div class=\"col-lg-3\">\n              <div id=\"cart_product_img\" class=\"text-center\">\n                <img src=\"assets/images/demo2.jpg\" class=\"img-fluid\">\n              </div>   \n            </div>\n\n            <div class=\"col-lg-7 cart_sub\">\n              <h1 class=\"cart_product_title\">Print ethno tank</h1>\n              <p id=\"cart_item_id\">#12343434380930</p>\n              <section id=\"cart_item_spec\">\n                <p><img src=\"assets/webfonts/raindrop-close-up.svg\" width=\"20px\"><span>Black mix</span></p>\n                <p><img src=\"assets/webfonts/shirt.svg\" width=\"20px\"><span>Small Regular</span></p>\n                <p><img src=\"assets/webfonts/shopping-cart.svg\" width=\"20px\"><span>2 Item(s)</span></p>\n              </section>\n            </div>\n\n            <div class=\"col-lg-2 cart_sub\">\n              <div  id=\"cart_item_price\">\n                <h2 >N200,000</h2>\n                <p><img src=\"assets/webfonts/cross-out.svg\" width=\"20px\"></p>\n              </div>\n              \n            </div>\n          </div> \n        </div>\n\n        <!-- *********** -->\n        <div class=\"container-fluid\" id=\"cart_item\">\n          <div class=\"row\">\n\n            <div class=\"col-lg-3\" >\n              <div id=\"cart_product_img\" class=\"text-center\">\n                <img src=\"assets/images/demo2.jpg\" class=\"img-fluid\">\n              </div>   \n            </div>\n\n            <div class=\"col-lg-7 cart_sub\">\n              <h1 class=\"cart_product_title\">Print ethno tank</h1>\n              <p id=\"cart_item_id\">#12343434380930</p>\n              <section id=\"cart_item_spec\">\n                <p><img src=\"assets/webfonts/raindrop-close-up.svg\" width=\"20px\"><span>Black mix</span></p>\n                <p><img src=\"assets/webfonts/shirt.svg\" width=\"20px\"><span>Small Regular</span></p>\n                <p><img src=\"assets/webfonts/shopping-cart.svg\" width=\"20px\"><span>2 Item(s)</span></p>\n              </section>\n            </div>\n\n            <div class=\"col-lg-2 cart_sub\">\n              <div id=\"cart_item_price\">\n                <h2 >N200,000</h2>\n                <p><img src=\"assets/webfonts/cross-out.svg\" width=\"20px\"></p>\n              </div>\n              \n            </div>\n          </div> \n        </div>\n\n       <!-- ///////////////////// discount ////////////////// -->\n        <div class=\"container-fluid\" id=\"cart_item\">\n          <div class=\"row\">\n            <div class=\"col-md-8\">\n              <img id=\"discount_img\" src=\"assets/webfonts/tag.svg\" width=\"30px\">\n              <div>\n                <h5>DISCOUNT / PROMO CODE</h5>\n                <p>Don't have any yet? <a href=\"\">check our discount programs </a>to try some luck.</p>\n              </div>\n              \n            </div>\n            <div class=\"col-md-4\">\n              <div class=\"box\">\n                <div class=\"container-2\">\n                  <form action=\"\" method=\"\">\n                    <a href=\"\"><span class=\"icon\">ENTER</span></a>\n                    <input type=\"search\" id=\"search\" placeholder=\"xxxxxxxxx\" />\n                  </form>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <!-- ///////////////////// end of discount ////////////////// -->\n        \n        <!-- /////////////// -->\n        <div class=\"container-fluid\" id=\"cart_item \">\n          <div class=\"row\">\n            <div class=\"col check_cart\">\n              <div class=\"row\">\n                <div class=\"col-md-7 ck_box\">\n                  <div class=\"row\">\n                    <div class=\"col-sm-1 no_padding\">\n                      <img id=\"check_out_img\" src=\"assets/webfonts/shopping-cart.svg\" width=\"20px\">\n                    </div> \n                    <div class=\"col-sm-11 \">\n                      <h5><b>ESTIMATION</b></h5>\n                      <!-- /////////// -->\n                      <div class=\"col no_padding\">\n                        <div class=\"row\">\n                          <div class=\"col-sm-6\">\n                            <div><p id=\"check_out\">Order</p></div>\n                          </div>\n                          <div class=\"col-sm-6\">\n                             <div class=\"expense\"><b>N138,000</b></div>\n                          </div>\n                        </div>\n                      </div>\n                      \n                      <!-- /////////// -->\n                      <div class=\"col no_padding\">\n                        <div class=\"row\">\n                          <div class=\"col-sm-6\">\n                            <div><p id=\"check_out\">3-5 Business days shipping</p></div>\n                          </div>\n                          <div class=\"col-sm-6\">\n                             <div class=\"expense\"><b>N2,000</b></div>\n                          </div>\n                        </div>\n                      </div>\n                      <!-- /////////// -->\n                      <div class=\"col no_padding\">\n                        <div class=\"row\">\n                          <div class=\"col-sm-6\">\n                            <div><p id=\"check_out\">Bonus/Discount</p></div>\n                          </div>\n                          <div class=\"col-sm-6\">\n                             <div class=\"expense\"><b>N1,000</b></div>\n                          </div>\n                        </div>\n                      </div>\n\n                      <!-- /////////// -->\n                      <div class=\"col no_padding\">\n                        <div class=\"row\">\n                          <div class=\"col-sm-6\">\n                            <div><p id=\"check_out\">Taxes</p></div>\n                          </div>\n                          <div class=\"col-sm-6\">\n                             <div class=\"expense\"><b>N10</b></div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>     \n                  </div>\n                  \n                            \n                </div>\n                <div class=\"col-md-5 ck_box\">\n                  <div class=\"text-right\">\n                    <h5>Total</h5>\n                    <br>\n                    <h2>N141,000</h2>\n                    <br>\n                    <a href=\"check_out.html\" class=\"btn btn-primary blue\">  PROCEED TO CHECK OUT  </a>\n                  </div>\n                  \n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n\n      </div> \n    <br>\n   </div>\n"

/***/ }),

/***/ "./src/app/cart/cart.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CartComponent = (function () {
    function CartComponent() {
    }
    CartComponent.prototype.ngOnInit = function () {
    };
    CartComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-cart',
            template: __webpack_require__("./src/app/cart/cart.component.html"),
            styles: [__webpack_require__("./src/app/cart/cart.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], CartComponent);
    return CartComponent;
}());



/***/ }),

/***/ "./src/app/cart/cart.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cart_component__ = __webpack_require__("./src/app/cart/cart.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared__ = __webpack_require__("./src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var cartRouting = __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild([
    {
        path: 'cart',
        component: __WEBPACK_IMPORTED_MODULE_2__cart_component__["a" /* CartComponent */]
    }
]);
var CartModule = (function () {
    function CartModule() {
    }
    CartModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                cartRouting,
                __WEBPACK_IMPORTED_MODULE_3__shared__["a" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__cart_component__["a" /* CartComponent */]
            ],
            providers: []
        })
    ], CartModule);
    return CartModule;
}());



/***/ }),

/***/ "./src/app/checkout/checkout.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/checkout/checkout.component.html":
/***/ (function(module, exports) {

module.exports = "\n<!-- ===========================================\n      Cart Content\n -->\n   <div id=\"content_wrapper\">\n    <br>\n      <div class=\"ct-page\">\n        <!--  -->\n        <div class=\"container-fluid\">\n          <div class=\"row\">\n            <div class=\"col\">\n            \n                <h2 class=\"cart-title text-center\">A FEW STEPS TO PLACE THIS ORDER</h2>\n                <br>\n                <!-- Check Out Tabs -->\n                <ul id=\"tablist\" class=\"check_out_tabs text-center\" role=\"tablist\">\n                  <li class=\"nav-item \">\n                    <a class=\"nav-link active\" data-toggle=\"\" href=\"#shipping\">Shipping Details</a>\n                  </li>\n                  <li class=\"nav-item\" >\n                    <i class=\"fa fa-circle\"></i><a class=\"nav-link\"  data-toggle=\"\" href=\"#payment\">Payment</a>\n                  </li>\n                  <li class=\"nav-item\" >\n                    <i class=\"fa fa-circle\"></i><a class=\"nav-link\"  data-toggle=\"\" href=\"#\">Confirmation</a>\n                  </li>\n                </ul>\n                <br>\n                <!--  -->\n                <div class=\"row\">\n                  <div class=\"col-4\">\n                  </div>\n                  <div class=\"col-4\">\n                    <div class=\"check_out_anchor text-left\">\n                     <h6><b>SAVE YOUR PROGRESS</b></h6>\n                     <p><a href=\"\">Login</a> to autocomplete required information and speed up checkout process.</p>\n                   </div>\n                  </div>\n                  <div class=\"col-4\"> \n                  </div>\n                </div>\n                \n                <hr width=\"20%\">\n\n                <!-- Tab panes -->\n                <div class=\"tab-content check_cart_content\">\n\n                  <!-- Shopping Details -->\n\n                  <div id=\"shopping-details\" class=\"container tab-pane active\">\n                     <div class=\"row\"> \n                       <div class=\"col-sm-2\">\n                         <h5><b>DELIVERY</b></h5>\n                       </div>\n                      </div>\n                       <div class=\"row\">\n                        <div class=\"col-sm-2\">\n                          \n                        </div>\n                       <div class=\"col-sm-8 up_padding\">\n                        <div class=\"form-check\">\n                          <label class=\"form-check-label fcl\">\n                              <input type=\"radio\" class=\"form-check-input\" name=\"optionsRadios\" id=\"optionsRadios2\" value=\"option2\">\n                              Standard Delivery 3-5 days\n                            </label>\n                          </div>\n                          <div class=\"hidden-sm-up fct\">\n                             <p><b>N2,000</b></p>\n                           </div>\n                          <br>\n                          <div class=\"form-check\">\n                          <label class=\"form-check-label fcl\">\n                              <input type=\"radio\" class=\"form-check-input\" name=\"optionsRadios\" id=\"optionsRadios3\" value=\"option3\">\n                              Next day &amp; evening delivery\n                              <small id=\"checkHelp\" class=\"form-text text-muted\">Order before 8pm for delivery tomorrow.</small>\n                           </label> \n                          </div>\n                          <div class=\"hidden-sm-up fct\">\n                             <p><b>N5,000</b></p>\n                           </div>\n                          <br>\n                          <div class=\"form-check\">\n                          <label class=\"form-check-label fcl\">\n                              <input type=\"radio\" class=\"form-check-input\" name=\"optionsRadios\" id=\"optionsRadios4\" value=\"option3\">\n                              Nominated day delivery\n                              <small id=\"checkHelp\" class=\"form-text text-muted\">Pick a date from the calendar.</small>\n                            </label>\n                          </div> \n                          <div class=\"hidden-sm-up fct\">\n                             <p><b>N10,000</b></p>\n                           </div>               \n                           \n                       </div>\n                       <div class=\"col-sm-2 up_padding hidden-xs-down\">\n                           <div>\n                             <p><b>N2,000</b></p>\n                           </div>\n                           <br>\n                           <div>\n                             <p><b>N5,000</b></p>\n                           </div>\n                           <br>\n                           <div>\n                             <p><b>N10,000</b></p>\n                           </div>\n\n                       </div>\n                     </div>\n                     <!--  -->\n\n                     <br><br>\n                     <div class=\"row\">\n                        <div class=\"col-sm-2 \">\n                         <h5><b>ADDRESS</b></h5>\n                       </div>\n                       <div class=\"col-sm-8-offset-2 up_padding\">\n                         <!--  -->\n                          <form>\n                             <div class=\"form-group\">\n                               <div class=\"form-check\">\n                                  <label class=\"form-check-label fcl-bolder\">\n                                    <input class=\"form-check-input\" type=\"checkbox\"> Billing addrerss is the same as Delivery address\n                                  </label>\n                               </div>\n                             </div>\n                             <br>\n                              <div class=\"form-group fcl2\">\n                                <label for=\"exampleInputEmail1\" class=\"\">Fullname</label>\n                                <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"\">\n                              </div>\n                              <div class=\"form-group fcl2\">\n                                <label for=\"exampleInputPassword1\" class=\"\">Zip/postcode</label>\n                                <input type=\"text\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\">\n                              </div>\n                               \n                              <div class=\"form-group\">\n                              <div class=\"form-group row fcl3\">\n                                  <div class=\"form-group col-md-4  \">\n                                    <label for=\"exampleInputEmail1\" class=\"\">House Number</label>\n                                    <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"\">\n                                  </div>\n                                  <div class=\"form-group col-md-8\">\n                                    <label for=\"exampleInputPassword1\" class=\"\">Street</label>\n                                    <input type=\"text\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\">\n                                  </div>\n                              </div>\n                              </div>\n\n                              <div class=\"form-group row fcl3\">\n                                  <div class=\"form-group col-md-7  \">\n                                    <label for=\"exampleInputEmail1\" class=\"\">State</label>\n                                    <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"\">\n                                  </div>\n                                  <div class=\"form-group col-md-5\">\n                                    <label for=\"exampleInputPassword1\" class=\"\">City</label>\n                                    <input type=\"text\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\">\n                                  </div>\n                              </div>\n\n                              \n                              <div class=\"form-group fcl2\">\n                                <label for=\"exampleInputPassword1\" class=\"\">Phone Number</label>\n                                <input type=\"text\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\">\n                              </div>\n                           \n                          </form>\n                         \n                       </div>\n                       \n                     </div>\n                     <br>\n\n                     <!-- /////////////// -->\n                    <div class=\"container-fluid\" id=\"cart_item \">\n                      <div class=\"row\">\n                        <div class=\"col check_cart\">\n                          <div class=\"row\">\n                            <div class=\"col-md-7 ck_box\">\n                              <div class=\"row\">\n                                <div class=\"col-sm-1 no_padding\">\n                                  <img id=\"check_out_img\" src=\"assets/webfonts/shopping-cart.svg\" width=\"20px\">\n                                </div> \n                                <div class=\"col-sm-11 \">\n                                  <h5><b>ESTIMATION</b></h5>\n                                  <!-- /////////// -->\n                                  <div class=\"col no_padding\">\n                                    <div class=\"row\">\n                                      <div class=\"col-sm-6\">\n                                        <div><p id=\"check_out\">Order</p></div>\n                                      </div>\n                                      <div class=\"col-sm-6\">\n                                         <div class=\"expense\"><b>N138,000</b></div>\n                                      </div>\n                                    </div>\n                                  </div>\n                                  \n                                  <!-- /////////// -->\n                                  <div class=\"col no_padding\">\n                                    <div class=\"row\">\n                                      <div class=\"col-sm-6\">\n                                        <div><p id=\"check_out\">3-5 Business days shipping</p></div>\n                                      </div>\n                                      <div class=\"col-sm-6\">\n                                         <div class=\"expense\"><b>N2,000</b></div>\n                                      </div>\n                                    </div>\n                                  </div>\n                                  <!-- /////////// -->\n                                  <div class=\"col no_padding\">\n                                    <div class=\"row\">\n                                      <div class=\"col-sm-6\">\n                                        <div><p id=\"check_out\">Bonus/Discount</p></div>\n                                      </div>\n                                      <div class=\"col-sm-6\">\n                                         <div class=\"expense\"><b>N1,000</b></div>\n                                      </div>\n                                    </div>\n                                  </div>\n\n                                  <!-- /////////// -->\n                                  <div class=\"col no_padding\">\n                                    <div class=\"row\">\n                                      <div class=\"col-sm-6\">\n                                        <div><p id=\"check_out\">Taxes</p></div>\n                                      </div>\n                                      <div class=\"col-sm-6\">\n                                         <div class=\"expense\"><b>N10</b></div>\n                                      </div>\n                                    </div>\n                                  </div>\n                                </div>     \n                              </div>                      \n                            </div>\n                            <div class=\"col-md-5 ck_box\">\n                              <div class=\"text-right\">\n                                <h5>Total</h5>\n                                <br>\n                                <h2>N141,000</h2>\n                                <br>\n                                <button id=\"nexxt\" data-toggle=\"tab\" href=\"#payment\" class=\"btn btn-primary blue\">  PROCEED TO PAYMENT  </button>\n                              </div>\n                              \n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n\n                  <!-- ******************************************************************\n                      Payment \n                   *************************************************************-->\n                  <div id=\"payment\" class=\"container tab-pane fade\"><br>\n                    <div class=\"row\"> \n                       <div class=\"col\">\n                         <h5><b>PROMPT PAYMENT</b></h5>\n                       </div>\n                      </div>\n                      <div class=\"row\">\n                        <div class=\"col-6\">\n                          <p>Pay using PayPal and Stripe account. You will be redirected to one of these systems respectively to complete payment.</p>\n                        </div>\n                     </div>\n                     <div class=\"row\">\n                       <div class=\"col-sm-5\">\n                         <div class=\"pay_img\">\n                           <img src=\"assets/images/demo4.png\" class=\"a img-fluid\">\n                         </div>\n                       </div>\n                       <div class=\"col-sm-2 pay_img\">\n                         <p class=\"text-center\">OR</p>\n                       </div>\n                       <div class=\"col-sm-5\">\n                         <div class=\"pay_img\">\n                           <img src=\"assets/images/demo5.png\" class=\"b img-fluid\">\n                         </div>\n                       </div>\n                     </div>\n                     <!--  -->\n\n                     <br><br>\n                     <div class=\"row\">\n                        <div class=\"col-12 \">\n                         <h5><b>CREDIT / DEBIT CARD PAYMENT</b></h5>\n                       </div>\n                       <div class=\"col-sm-2 \">\n                       </div>\n                       <div class=\"col-sm-8 up_padding\">\n                         <!--  -->\n                          <form>  \n                             <br>\n                              <div class=\"form-group fcl2\">\n                                <label for=\"exampleInputEmail1\" class=\"\">Name on card</label>\n                                <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"\">\n                              </div>\n                              <div class=\"form-group fcl2\">\n                                <label for=\"exampleInputPassword1\" class=\"\">Card Number</label>\n                                <input type=\"text\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\">\n                              </div>\n                               \n                              <div class=\"form-group\">\n                              <div class=\"form-group row fcl3\">\n                                  <div class=\"form-group col-md-4  \">\n                                    <label for=\"exampleInputEmail1\" class=\"\">Expiry Date</label>\n                                    <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"\">\n                                  </div>\n                                  <div class=\"form-group col-md-8\">\n                                    <label for=\"exampleInputPassword1\" class=\"\">Security Code</label>\n                                    <input type=\"text\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\">\n                                  </div>\n                              </div>\n                              </div>\n                              \n                              <div class=\"form-group fcl2\">\n                                <label for=\"exampleInputPassword1\" class=\"\">Zip/postcode</label>\n                                <input type=\"text\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\"> \n                                <div id=\"footnote\">\n                                  <small>By clicking <b>PAY NOW SECURELY</b> you agree to our <a href=\"\">Terms and Conditions</a></small>\n                                </div>\n                              </div>   \n                          </form>\n                          <br>\n                          \n                         \n                       </div>\n                       <div class=\"col-sm-2 \">\n                       </div>\n                       \n                     </div>\n                     <br>\n\n                     <!-- /////////////// -->\n                    <div class=\"container-fluid\" id=\"cart_item \">\n                      <div class=\"row\">\n                        <div class=\"col check_cart\">\n                          <div class=\"row\">\n                            <div class=\"col-md-7 ck_box\">\n                              <div class=\"row\">\n                                <div class=\"col-sm-1 no_padding\">\n                                  <img id=\"check_out_img\" src=\"assets/webfonts/shopping-cart.svg\" width=\"20px\">\n                                </div> \n                                <div class=\"col-sm-11 \">\n                                  <h5><b>ESTIMATION</b></h5>\n                                  <!-- /////////// -->\n                                  <div class=\"col no_padding\">\n                                    <div class=\"row\">\n                                      <div class=\"col-sm-6\">\n                                        <div><p id=\"check_out\">Order</p></div>\n                                      </div>\n                                      <div class=\"col-sm-6\">\n                                         <div class=\"expense\"><b>N138,000</b></div>\n                                      </div>\n                                    </div>\n                                  </div>\n                                  \n                                  <!-- /////////// -->\n                                  <div class=\"col no_padding\">\n                                    <div class=\"row\">\n                                      <div class=\"col-sm-6\">\n                                        <div><p id=\"check_out\">3-5 Business days shipping</p></div>\n                                      </div>\n                                      <div class=\"col-sm-6\">\n                                         <div class=\"expense\"><b>N2,000</b></div>\n                                      </div>\n                                    </div>\n                                  </div>\n                                  <!-- /////////// -->\n                                  <div class=\"col no_padding\">\n                                    <div class=\"row\">\n                                      <div class=\"col-sm-6\">\n                                        <div><p id=\"check_out\">Bonus/Discount</p></div>\n                                      </div>\n                                      <div class=\"col-sm-6\">\n                                         <div class=\"expense\"><b>N1,000</b></div>\n                                      </div>\n                                    </div>\n                                  </div>\n\n                                  <!-- /////////// -->\n                                  <div class=\"col no_padding\">\n                                    <div class=\"row\">\n                                      <div class=\"col-sm-6\">\n                                        <div><p id=\"check_out\">Taxes</p></div>\n                                      </div>\n                                      <div class=\"col-sm-6\">\n                                         <div class=\"expense\"><b>N10</b></div>\n                                      </div>\n                                    </div>\n                                  </div>\n                                </div>     \n                              </div>\n                              \n                                        \n                            </div>\n                            <div class=\"col-md-5 ck_box\">\n                              <div class=\"text-right\">\n                                <h5>Total</h5>\n                                <br>\n                                <h2>N141,000</h2>\n                                <br>\n                                <a href=\"confirmation.html\" class=\"btn btn-primary blue\">   PAY NOW SECURELY </a>\n                              </div>\n                              \n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                  </div>\n\n                  <!--****************************************8\n                        Confirmation\n                    ********************************************* -->\n                  <div id=\"confirmation\" class=\"container tab-pane fade\"><br>\n                    <h3>Menu 2</h3>\n                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>\n                  </div>\n                </div>\n          \n            </div>\n          </div>\n        </div>\n        \n        \n\n      </div> \n    <br>\n   </div>"

/***/ }),

/***/ "./src/app/checkout/checkout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CheckoutComponent = (function () {
    function CheckoutComponent() {
    }
    CheckoutComponent.prototype.ngOnInit = function () {
    };
    CheckoutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-checkout',
            template: __webpack_require__("./src/app/checkout/checkout.component.html"),
            styles: [__webpack_require__("./src/app/checkout/checkout.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], CheckoutComponent);
    return CheckoutComponent;
}());



/***/ }),

/***/ "./src/app/checkout/checkout.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckoutModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__checkout_component__ = __webpack_require__("./src/app/checkout/checkout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared__ = __webpack_require__("./src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var checkoutRouting = __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild([
    {
        path: 'checkout',
        component: __WEBPACK_IMPORTED_MODULE_2__checkout_component__["a" /* CheckoutComponent */]
    }
]);
var CheckoutModule = (function () {
    function CheckoutModule() {
    }
    CheckoutModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                checkoutRouting,
                __WEBPACK_IMPORTED_MODULE_3__shared__["a" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__checkout_component__["a" /* CheckoutComponent */]
            ],
            providers: []
        })
    ], CheckoutModule);
    return CheckoutModule;
}());



/***/ }),

/***/ "./src/app/home/Banner/banner.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BannerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/_esm5/Rx.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BannerService = (function () {
    function BannerService(http) {
        this.http = http;
        this._http = http;
    }
    BannerService.prototype.getBanners = function () {
        return this._http.get('http://magento.amalinze.com/api/getBanners.php');
    };
    BannerService.prototype.getToyGames = function () {
        return this._http.get('http://magento.amalinze.com/api/getCategoryProducts.php?id=3');
    };
    BannerService.prototype.getHomeKitchen = function () {
        return this._http.get('http://magento.amalinze.com/api/getCategoryProducts.php?id=4');
    };
    BannerService.prototype.getOffers = function () {
        return this._http.get('http://magento.amalinze.com/api/getCategoryProducts.php?id=5');
    };
    BannerService.prototype.getRecommend = function () {
        return this._http.get('http://magento.amalinze.com/api/getCategoryProducts.php?id=6');
    };
    BannerService.prototype._handleError = function (error) {
        console.error(error.json());
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(error.json() || 'Server error');
    };
    BannerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], BannerService);
    return BannerService;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/***/ (function(module, exports) {

module.exports = "\r\n #product_title p {\r\n        max-width: 220px;\r\n        color: #343434;\r\n        font-weight: bold;\r\n        white-space: nowrap;\r\n        overflow: hidden;\r\n        padding: 5px;\r\n        text-overflow: ellipsis;\r\n   }\r\n\r\n@media(max-width: 768px){\r\n\r\n  .product_cat{\r\n      padding: 10px;\r\n  }\r\n\r\n  .mobile_cat_view {\r\n  \t  float: left;\r\n   }\r\n\r\n   .mobile_cat_view h4{\r\n   \t   font-size: 15px;\r\n   }\r\n\r\n   #product_cat_view img{\r\n   \t width: 150px;\r\n   \t height: 100px;\r\n   }\r\n\r\n\r\n   .red {\r\n    background: #ff7473;\r\n    text-transform: uppercase;\r\n    border: none;\r\n    font-size: 10px;\r\n    padding: 10px 20px 10px 20px;\r\n    }\r\n\r\n    .blue{\r\n\t  background: #5bc0de;\r\n\t  text-transform: uppercase;\r\n\t  border: none;\r\n\t  font-size: 10px;\r\n      padding: 10px 20px 10px 20px;\r\n\t}\r\n\r\n\t.purple{\r\n\t  background: #2e294c;\r\n\t  text-transform: uppercase;\r\n\t  border: none;\r\n\t  font-size: 10px;\r\n      padding: 10px 20px 10px 20px;\r\n      }\r\n\r\n    .orange{\r\n\t  background: #fba935;\r\n\t  text-transform: uppercase;\r\n\t  border: none;\r\n\t  font-size: 10px;\r\n      padding: 10px 20px 10px 20px;\r\n\t}\r\n}"

/***/ }),

/***/ "./src/app/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- =========== Slider Bootstrap Carousel -->\n    <div class=\"slider_wrapper\">  \n        <div class=\"container-fluid carousel_header\">\n          <div class=\"row\">\n            <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">\n              <div class=\"carousel-inner\" role=\"listbox\">\n                <div class=\"carousel-item active\">\n                  <img class=\"d-block img-fluid\" src=\"assets/images/demo-slider-2.jpg\" alt=\"Second slide\">\n                  <div class=\"overlay_carousel\"></div>\n                  <div class=\"carousel-cap\">\n                    <h1 class=\"text-center\">20% Off </h1>\n                    <h2 class=\"text-center\">Best Shoe Deals</h2> <br>\n                    <div id=\"action_btn\">\n                      <a class=\"btn  btn-primary btn-lg white\" href=\"/category\">Shop Now</a>\n                    </div>\n                  </div>\n                </div>\n\n                <div class=\"carousel-item\" *ngFor=\"let item of slides\">\n                  \n                  <img class=\"d-block img-fluid\" src=\"{{ item.image }}\" alt=\"First slide\">\n                  <div class=\"overlay_carousel\"></div>\n                  <div class=\"carousel-cap\">\n                    <h1 class=\"text-center\">20% Off </h1>\n                    <h2 class=\"text-center\">Best Shoe Deals</h2> <br>\n                    <div id=\"action_btn\">\n                      <a class=\"btn  btn-primary btn-lg white\" href=\"category\">Shop Now</a>\n                    </div> \n                  </div>\n                  \n                </div>\n                \n                \n              </div>\n            </div>\n          </div>\n        </div>\n    </div> \n    <!-- =========== End of Slider Bootstrap Carousel =============== -->\n<!-- =================== Product Category ============= -->\n     <div class=\"product_wrapper\">\n\n      <div class=\" container-fluid\">\n         <!-- First category -->\n          <div class=\"row \">\n              <div class=\"product_cat col-md-3\">\n                <div id=\"product_cat_view\">\n                  <div class=\"hidden-md-up\">\n                     <img src=\"assets/images/toy.jpg\" class=\"float-right\">\n                  </div>\n\n                  <div class=\"mobile_cat_view\">\n                     <h4 class=\"text-center\">Toys &amp; Games</h4><br>\n                      <div id=\"btn_toy\">\n                        <a class=\"btn  btn-primary red\">view all</a>\n                      </div>\n                  </div>\n                  \n              </div>\n              </div>\n              <div class=\" product_cat col-md-9 hidden-sm-down\">\n               <div class=\"container p8\">   \n                <div class=\"resCarousel\" data-items=\"2-3-4-5\" data-slide=\"5\" data-speed=\"900\" data-interval=\"none\" data-load=\"3\" data-animator=\"lazy\">\n                    <div class=\"resCarousel-inner\" id=\"eventLoad\">\n\n                        <div class=\"item\" *ngFor=\"let item of toys\">\n                            <div class=\"tile\">\n                                <div>\n                                    <img src=\"{{ item.image_path }}\" class=\"img-fluid\" (click)=\"fetchProductModalContent(item.id)\">\n                                </div>\n                               \n                            </div>\n                        </div>\n\n                        \n\n                    </div>\n                    <button class='btn btn-default leftRs'><i class=\"fa fa-chevron-left\"></i></button>\n                    <button class='btn btn-default rightRs'><i class=\"fa fa-chevron-right\"></i></button>\n                </div>\n               </div> \n              </div>\n            </div>\n            <br>\n\n        \n\n        <!-- Home & Kitchen category -->\n             <div class=\"row \">\n              <div class=\"product_cat col-md-3\">\n                <div id=\"product_cat_view\">\n                  <div class=\"hidden-md-up\">\n                     <img src=\"assets/images/kitchen.jpg\" class=\"float-right\">\n                  </div>\n                  <div class=\"mobile_cat_view\">\n                  <h4 class=\"text-center\">Home &amp; Kitchen</h4><br>\n                      <div id=\"btn_toy\">\n                        <a class=\"btn  btn-primary blue\">view all</a>\n                      </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\" product_cat col-md-9 hidden-sm-down\">\n                \n              \n               <div class=\"container p8\">   \n                <div class=\"resCarousel\" data-items=\"2-3-4-5\" data-slide=\"5\" data-speed=\"900\" data-interval=\"none\" data-load=\"3\" data-animator=\"lazy\">\n                    <div class=\"resCarousel-inner\" id=\"eventLoad\">\n\n                        <div class=\"item\" *ngFor=\"let item of homes\">\n                            <div class=\"tile\">\n                                <div>\n                                    <img src=\"{{ item.image_path }}\" class=\"img-fluid\"  (click)=\"fetchProductModalContent(item.id)\" />\n                                </div>\n                               \n                            </div>\n                        </div>\n\n                        \n\n                    </div>\n                    <button class='btn btn-default leftRs'><i class=\"fa fa-chevron-left\"></i></button>\n                    <button class='btn btn-default rightRs'><i class=\"fa fa-chevron-right\"></i></button>\n                </div>\n               </div> \n              \n                \n              </div>\n            </div>\n            <br>\n             <!-- Offers for you category -->\n             <div class=\"row \">\n              <div class=\"product_cat col-md-3\">\n                <div id=\"product_cat_view\">\n                  <div class=\"hidden-md-up\">\n                     <img src=\"assets/images/phone.png\" class=\"float-right\">\n                  </div>\n                  <div class=\"mobile_cat_view\">\n                    <h4 class=\"text-center\">Offers for you</h4><br>\n                    <div id=\"btn_toy\">\n                      <a class=\"btn  btn-primary purple\">view all</a>\n                    </div>\n                  </div>\n                 </div>\n              </div>\n              <div class=\" product_cat col-md-9 hidden-sm-down\">\n                \n                \n               <div class=\"container p8\">   \n                <div class=\"resCarousel\" data-items=\"2-3-4-5\" data-slide=\"5\" data-speed=\"900\" data-interval=\"none\" data-load=\"3\" data-animator=\"lazy\">\n                    <div class=\"resCarousel-inner\" id=\"eventLoad\">\n\n                        <div class=\"item\" *ngFor=\"let item of offers\">\n                            <div class=\"tile\">\n                                <div>\n                                    <img src=\"{{ item.image_path }}\" class=\"img-fluid\" (click)=\"fetchProductModalContent(item.id)\">\n                                </div>\n                               \n                            </div>\n                        </div>\n\n                        \n\n                    </div>\n                    <button class='btn btn-default leftRs'><i class=\"fa fa-chevron-left\"></i></button>\n                    <button class='btn btn-default rightRs'><i class=\"fa fa-chevron-right\"></i></button>\n                \n               </div> \n              </div>\n              </div>\n            </div>\n            <br>\n            \n            <!-- Recommended for you category -->\n            <div class=\"row \">\n              <div class=\"product_cat col-md-3\">\n                <div id=\"product_cat_view\">\n                  <div class=\"hidden-md-up\">\n                    <img src=\"assets/images/shoe.jpg\" class=\"float-right\">\n                  </div>\n                  <div class=\"mobile_cat_view\">\n                    <h4 class=\"text-center\">Recommended for you</h4><br>\n                    <div id=\"btn_toy\">\n                      <a class=\"btn  btn-primary orange\">view all</a>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\" product_cat col-md-9 hidden-sm-down\">\n\n\n                <div class=\"container p8\">\n                  <div class=\"resCarousel\" data-items=\"2-3-4-5\" data-slide=\"5\" data-speed=\"900\" data-interval=\"none\" data-load=\"3\" data-animator=\"lazy\">\n                    <div class=\"resCarousel-inner\" id=\"eventLoad\">\n\n                      <div class=\"item\" *ngFor=\"let item of recommended\">\n                        <div class=\"tile\">\n                          <div>\n                            <img src=\"{{ item.image_path }}\" class=\"img-fluid\" (click)=\"fetchProductModalContent(item.id)\">\n                          </div>\n\n                        </div>\n                      </div>\n\n\n\n                    </div>\n                    <button class='btn btn-default leftRs'><i class=\"fa fa-chevron-left\"></i></button>\n                    <button class='btn btn-default rightRs'><i class=\"fa fa-chevron-right\"></i></button>\n                  </div>\n                </div>\n\n\n              </div>\n            </div>\n            <br>\n\n        </div>\n      </div> \n     \n\n<!-- Product Category Modal -->\n    <div class=\"modal fade\" id=\"exampleModalLong\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\" aria-hidden=\"true\">\n      <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n          <div class=\"modal-header\">\n            <h5 class=\"modal-title\" id=\"exampleModalLongTitle\"></h5>\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n              <span aria-hidden=\"true\"><svg width=\"20px\" heiht=\"20px\" version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n   viewBox=\"0 0 224.512 224.512\" style=\"enable-background:new 0 0 224.512 224.512;\" xml:space=\"preserve\">\n<g>\n  <polygon style=\"fill:#010002;\" points=\"224.507,6.997 217.521,0 112.256,105.258 6.998,0 0.005,6.997 105.263,112.254 \n    0.005,217.512 6.998,224.512 112.256,119.24 217.521,224.512 224.507,217.512 119.249,112.254  \"/>\n</g>\n<g>\n</g>\n<g>\n</g>\n<g>\n</g>\n<g>\n</g>\n<g>\n</g>\n<g>\n</g>\n<g>\n</g>\n<g>\n</g>\n<g>\n</g>\n<g>\n</g>\n<g>\n</g>\n<g>\n</g>\n<g>\n</g>\n<g>\n</g>\n<g>\n</g>\n</svg></span>\n            </button>\n          </div>\n          <div class=\"modal-body\">\n            <div class=\"container\">\n              <div class=\"row\">\n                <div class=\"col-xs-5\" id=\"product_img\">\n                  <div >\n                    <img src=\"{{currentProductModalImage}}\" >\n                  </div>   \n                </div>\n                <div class=\"col-xs-7 \" id=\"product_description\">\n                  <div id=\"product_title\">\n                    <p>{{currentProductModalName}}</p>\n                  </div>\n                  <div >\n                        \n                  </div>\n                  <div id=\"rating\">\n                    \n                    <p><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half\"></i></p>\n                    <p id=\"reviews\">4,300</p>\n                  </div>\n                  <div >\n                    <p id=\"price\">N{{currentProductModalPrice}}</p>\n                  </div>\n                  <button class=\"btn btn-primary blue\">See product details</button>\n                </div>\n              </div>\n            </div>\n          </div>\n          \n        </div>\n      </div>\n    </div>\n\n<app-services></app-services>\n\n"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Banner_banner_service__ = __webpack_require__("./src/app/home/Banner/banner.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomeComponent = (function () {
    function HomeComponent(bannerService) {
        var _this = this;
        this.bannerService = bannerService;
        this.toys = [];
        this.homes = [];
        this.offers = [];
        this.slides = [];
        this.recommended = [];
        this.currentProductModalName = '';
        this.currentProductModalPrice = 0.00;
        this.currentProductModalImage = '';
        // Banner Service
        bannerService.getBanners().subscribe(function (response) {
            var data = response.json();
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var banner = data_1[_i];
                _this.slides.push(banner);
            }
        });
        // Banner Toy and Games Service
        bannerService.getToyGames().subscribe(function (response) {
            var data = response.json();
            for (var _i = 0, data_2 = data; _i < data_2.length; _i++) {
                var toy = data_2[_i];
                _this.toys.push(toy);
            }
        });
        // Banner Home and Kitchen Service
        bannerService.getHomeKitchen().subscribe(function (response) {
            var data = response.json();
            for (var _i = 0, data_3 = data; _i < data_3.length; _i++) {
                var kitchen = data_3[_i];
                _this.homes.push(kitchen);
            }
        });
        // Banner Offers Service
        bannerService.getOffers().subscribe(function (response) {
            var data = response.json();
            for (var _i = 0, data_4 = data; _i < data_4.length; _i++) {
                var offer = data_4[_i];
                _this.offers.push(offer);
            }
        });
        // Banner Recommended Service
        bannerService.getRecommend().subscribe(function (response) {
            var data = response.json();
            for (var _i = 0, data_5 = data; _i < data_5.length; _i++) {
                var recommend = data_5[_i];
                _this.recommended.push(recommend);
            }
        });
    }
    HomeComponent.prototype.fetchProductModalContent = function (product_id) {
        var toysArray = this.toys;
        for (var _i = 0, toysArray_1 = toysArray; _i < toysArray_1.length; _i++) {
            var toyproduct = toysArray_1[_i];
            if (toyproduct.id === product_id) {
                this.currentProductModalName = toyproduct.name;
                this.currentProductModalImage = toyproduct.image_path;
                this.currentProductModalPrice = toyproduct.price;
                console.log('KP ' + this.currentProductModalName);
                jQuery('#exampleModalLong').modal('show');
            }
        }
    };
    HomeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-home',
            template: __webpack_require__("./src/app/home/home.component.html"),
            styles: [__webpack_require__("./src/app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__Banner_banner_service__["a" /* BannerService */]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/home/home.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_services_component__ = __webpack_require__("./src/app/services/services.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_component__ = __webpack_require__("./src/app/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared__ = __webpack_require__("./src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var homeRouting = __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild([
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_3__home_component__["a" /* HomeComponent */]
    }
]);
var HomeModule = (function () {
    function HomeModule() {
    }
    HomeModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                homeRouting,
                __WEBPACK_IMPORTED_MODULE_4__shared__["a" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__home_component__["a" /* HomeComponent */],
                __WEBPACK_IMPORTED_MODULE_2__services_services_component__["a" /* ServicesComponent */]
            ],
            providers: []
        })
    ], HomeModule);
    return HomeModule;
}());



/***/ }),

/***/ "./src/app/loginpage/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/_esm5/Rx.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthService = (function () {
    function AuthService(http) {
        this.http = http;
        this.isLoginSubject = new __WEBPACK_IMPORTED_MODULE_1_rxjs__["BehaviorSubject"](this.hasToken());
    }
    /**
     *
     * @returns {Observable<T>}
     */
    AuthService.prototype.isLoggedIn = function () {
        return this.isLoginSubject.asObservable();
    };
    /**
     *  Login the user then tell all the subscribers about the new status
     */
    AuthService.prototype.login = function (username, password) {
        var body = JSON.stringify({
            "username": username,
            "password": password
        });
        return this.http.post('http://magento.amalinze.com/api/loginCustomer.php', body).map(function (response) {
            var data = response.json();
            return data;
        }).catch(function (error) {
            return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].throw('Something went wrong');
        });
    };
    /**
     * Log out the user then tell all the subscribers about the new status
     */
    AuthService.prototype.logout = function () {
        localStorage.clear();
        this.isLoginSubject.next(false);
    };
    /**
     * if we have token the user is loggedIn
     * @returns {boolean}
     */
    AuthService.prototype.hasToken = function () {
        return !!localStorage.getItem('email');
    };
    AuthService.prototype.ifLoggedIn = function () {
        return !!localStorage.getItem('email');
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/loginpage/loginpage.component.css":
/***/ (function(module, exports) {

module.exports = "\n\n#content*{\n    width: 100%;\n    padding: 0px;\n    min-height: 100vh;\n    -webkit-transition: all 0.3s;\n    transition: all 0.3s;\n    position: absolute;\n    top: 0;\n    right: 0;\n}\n\n#side_img {\n  margin-left: -20px;\n  width: 100%;\n  height: 100vh;\n  background: url('side.dc844fd8683d2344884d.png') no-repeat;\n  background-size: cover;\n  background-position: center;\n}\n\n#sign_up_btn {\n  text-align: center;\n  padding-top: 15px;\n}\n\n#sign_up_btn p {\n   padding-top: 10px;\n   font-size: 12px;\n}\n\n#login_text{\n  padding-top: 150px;\n  padding-right: 20px;\n  padding-left: 20px;\n  color: #fff;\n}\n\n#login_text h4 {\n  font-weight: bold;\n  margin-bottom: 14px;\n  padding-left: 10px;\n  text-align: left;\n  color: #fff;\n}\n\n#sign_up_btn a {\n  color: #ff7473;\n}\n\n#login_text p {\n  color: #fff;\n  padding-left: 10px;\n  font-size: 16px;\n}\n\n#login_text_mobile{\n  /*padding-top: 150px;*/\n  color: #343434;\n}\n\n#login_text_mobile h4 {\n  font-weight: bold;\n  margin-bottom: 10px;\n  text-align: left;\n  color: #343434;\n}\n\n#login_text_mobile p {\n  color: #343434;\n  font-size: 10px;\n}\n\n#logo_img{\n  width: 280px;\n  height: 60px;\n  margin: 0 auto;\n  margin-bottom: 50px;\n  background: url('amalinze_logo_sm.4a2fe8b2be6fe491a3a0.png') no-repeat center center / contain scroll rgb(255, 255, 255); \n}\n\n.form_wrapper {\n\tpadding-top: 40px;\n    padding-right: 20px;\n    padding-left: 20px;\n    margin: 0 auto;\n    width: 75vh;\n}\n\n.blue_sign{\n  background: #18b8e5;\n  color: #fff;\n  -ms-flex-line-pack: center;\n      align-content: center;\n  width: 100%;\n  padding-top: 11px;\n  padding-bottom: 11px;\n  border: none;\n}\n\n.blue_sign:hover{\n  background: #18b8e7;\n  border: none;\n}"

/***/ }),

/***/ "./src/app/loginpage/loginpage.component.html":
/***/ (function(module, exports) {

module.exports = "            \t\n            <div class=\"container-fluid\" id=\"login_anchor\">\n              <div class=\"row\">\n              \t<!-- -->\n\n                <div class=\"col-sm-4 no_padding\" >\n                  <div id=\"side_img\" class=\"hidden-sm-down\">\n                    <section id=\"login_text\">\n                    <h4>Login</h4>\n                    <p>Get your acess to your Orders, Wishlist and Recommendations.</p>\n                    </section>\n                  </div>\n                </div> \n\n                <div class=\"col-md-8\" id=\"login\">   \n                 <div class=\"form_wrapper\">\n                   <a href=\"/\">\n                     <div id=\"logo_img\"></div>\n                   </a>\n                    <section id=\"login_text_mobile\" class=\"hidden-md-up\">\n                    <h4>Login</h4>\n                    <p>Get your acess to your Orders, Wishlist and Recommendations.</p>\n                    </section> \n                    <div class=\"alert alert-danger\" *ngIf=\"isLoginError\">{{login_error}}</div>\n                    <div  *ngIf=\"canLoad\"><img src=\"assets/images/loader.gif\"/></div>\n\n                   <form id=\"loginForm\" name=\"loginForm\" #loginForm=\"ngForm\"  (ngSubmit)=\"onSubmit()\">\n                        <div class=\"form-group\">\n                      <label for=\"email\" >Email address</label>\n                      <input type=\"email\"\n                       id=\"email\"\n                        name=\"email\" \n                        class=\"form-control\" \n                         aria-describedby=\"emailHelp\" \n                         ngModel\n                         required\n                           email\n                          #email=\"ngModel\">\n                          <span class=\"help-block\" *ngIf=\"!email.valid && email.touched\">Please enter a valid Email!</span>\n                      \n                    </div>\n                    <div class=\"form-group\">\n                      <label for=\"password\" >Password</label>\n                      <input \n                      type=\"Password\"\n                       id=\"password\" \n                       name=\"password\" \n                       class=\"form-control\" \n                       ngModel\n                       required\n                      #password=\"ngModel\">\n                          <span class=\"help-block\" *ngIf=\"!password.valid && password.touched\">Please enter a valid Password!</span>\n                    </div>\n\n                    <div id=\"sign_up_btn\">\n                       <button class=\"btn btn-primary blue_sign\" [disabled]=\"!loginForm.valid\">Log in</button>\n                       <p>Don't have an account? <a  id=\"show_signup\" href=\"/signup\">Sign Up</a></p>\n                    </div>\n\n                  </form>\n                 </div>\n                \n              </div>    \n          </div>\n        </div>\n\n \n             \n\n\n            "

/***/ }),

/***/ "./src/app/loginpage/loginpage.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginpageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__("./src/app/loginpage/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginpageComponent = (function () {
    function LoginpageComponent(authService, router, location) {
        this.authService = authService;
        this.router = router;
        this.location = location;
        this.canLoad = false;
        this.isLoginError = false;
        this.login_error = "";
    }
    LoginpageComponent.prototype.onSubmit = function () {
        var _this = this;
        this.canLoad = true;
        this.authService.login(this.form.value.email, this.form.value.password).subscribe(function (response) {
            _this.canLoad = false;
            if (response.status_code == 200) {
                localStorage.setItem('email', _this.form.value.email);
                localStorage.setItem('firstname', response.firstname);
                localStorage.setItem('customer_id', response.id);
                _this.authService.isLoginSubject.next(true);
                _this.location.back();
            }
            else {
                _this.displayErrors(response.message);
            }
        }, function (error) {
            _this.canLoad = false;
        });
    };
    LoginpageComponent.prototype.ngOnInit = function () {
        if (this.authService.ifLoggedIn())
            location.href = '/';
    };
    LoginpageComponent.prototype.displayErrors = function (error) {
        this.isLoginError = true;
        this.login_error = error;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('loginForm'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* NgForm */])
    ], LoginpageComponent.prototype, "form", void 0);
    LoginpageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-loginpage',
            template: __webpack_require__("./src/app/loginpage/loginpage.component.html"),
            styles: [__webpack_require__("./src/app/loginpage/loginpage.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_4__angular_common__["f" /* Location */]])
    ], LoginpageComponent);
    return LoginpageComponent;
}());



/***/ }),

/***/ "./src/app/models/customer.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Customer; });
var Customer = (function () {
    function Customer(firstname, lastname, email, password, confirm) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.confirm = confirm;
    }
    return Customer;
}());



/***/ }),

/***/ "./src/app/order-confirmation/order-confirmation.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/order-confirmation/order-confirmation.component.html":
/***/ (function(module, exports) {

module.exports = "\n   <div id=\"content_wrapper\">\n    <br>\n      <div class=\"ct-page\">\n        <!--  -->\n        <div class=\"container-fluid\">\n          <div class=\"row\">\n            <div class=\"col tk_page\">\n             <section >\n                <div class=\"text-center\">\n                  <svg width=\"78px\" height=\"78px\" viewBox=\"0 0 78 78\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n                    <!-- Generator: Sketch 48.2 (47327) - http://www.bohemiancoding.com/sketch -->\n                    <title>bag</title>\n                    <desc>Created with Sketch.</desc>\n                    <defs></defs>\n                    <g id=\"Ecommerce-Checkout-Page\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\" transform=\"translate(-732.000000, -287.000000)\">\n                        <g id=\"Group-54\" transform=\"translate(343.000000, 287.000000)\" fill-rule=\"nonzero\">\n                            <g id=\"Group-53\" transform=\"translate(250.000000, 0.000000)\">\n                                <g id=\"bag\" transform=\"translate(139.000000, 0.000000)\">\n                                    <path d=\"M46.55,20 L2.45,20 C1.0976,20 0,21.0826667 0,22.4166667 L0,68.3333333 C0,73.6645 4.3953,78 9.8,78 L46.55,78 C47.9024,78 49,76.9173333 49,75.5833333 L49,22.4166667 C49,21.0826667 47.9024,20 46.55,20 Z\" id=\"Shape\" fill=\"#FF7473\"></path>\n                                    <path d=\"M75.5555556,39 L36.4444444,39 C35.0951111,39 34,40.092 34,41.4375 L34,68.25 C34,73.627125 38.3853333,78 43.7777778,78 L68.2222222,78 C73.6146667,78 78,73.627125 78,68.25 L78,41.4375 C78,40.092 76.9048889,39 75.5555556,39 Z\" id=\"Shape\" fill=\"#47B8E0\"></path>\n                                    <g id=\"Group\" transform=\"translate(10.000000, 0.000000)\" fill=\"#455A64\">\n                                        <path d=\"M26.5833333,29.4 C25.2493333,29.4 24.1666667,28.3024 24.1666667,26.95 L24.1666667,14.7 C24.1666667,9.2953 19.8311667,4.9 14.5,4.9 C9.16883333,4.9 4.83333333,9.2953 4.83333333,14.7 L4.83333333,26.95 C4.83333333,28.3024 3.75066667,29.4 2.41666667,29.4 C1.08266667,29.4 0,28.3024 0,26.95 L0,14.7 C0,6.5954 6.50566667,0 14.5,0 C22.4943333,0 29,6.5954 29,14.7 L29,26.95 C29,28.3024 27.9173333,29.4 26.5833333,29.4 Z\" id=\"Shape\"></path>\n                                        <path d=\"M55.5833333,49 C54.2493333,49 53.1666667,47.9024 53.1666667,46.55 L53.1666667,36.75 C53.1666667,32.6977 49.9138333,29.4 45.9166667,29.4 C41.9195,29.4 38.6666667,32.6977 38.6666667,36.75 L38.6666667,46.55 C38.6666667,47.9024 37.584,49 36.25,49 C34.916,49 33.8333333,47.9024 33.8333333,46.55 L33.8333333,36.75 C33.8333333,29.9978 39.2515,24.5 45.9166667,24.5 C52.5818333,24.5 58,29.9978 58,36.75 L58,46.55 C58,47.9024 56.9173333,49 55.5833333,49 Z\" id=\"Shape\"></path>\n                                    </g>\n                                </g>\n                            </g>\n                        </g>\n                    </g>\n                </svg>\n                </div>\n                <br>\n                <h4 class=\"text-center\">Thank you, Jane!</h4>\n                <!--  -->\n                <p class=\"text-center\" style=\"margin-bottom: 0px;\n\">Woot! Your order has been successfully completed</p>\n                <p class=\"text-center\">We received your payment and wrapping up stuff for shipping</p>\n                <br>\n                <!--  -->\n               \n\n                 \n                 <!-- ******************************************************************\n                      Order Details \n                   *************************************************************-->\n                 <div class=\"row\"> \n                       <div class=\"col\">\n                         <h5><b>ORDER DETAILS</b></h5>\n                       </div>\n                  </div>\n                  <!--  -->\n                  <div class=\"up_padding_tk\">\n                    <!-- First Order Detail -->\n                    <br>\n                    <div class=\"row\">\n                     <div class=\"col-sm-2\">    \n                      </div>\n                       <div class=\"col-sm-6 \">\n                         <div class=\"item_order\">\n                            <p><b>Print ethno tank</b></p>\n                         </div>\n                          <!--  -->\n                           <div class=\"row\">\n                               <div class=\"col order_number\">\n                                 <p>#1232323243</p>\n                               </div>\n                            </div>\n                           <div class=\"row\">\n                              <div class=\"col-sm-4\"> \n                                  <div id=\"order_detail\">\n                                     <p>Color: <span>Black</span></p>\n                                  </div>\n                              </div>\n\n                              <div class=\"col-sm-4\"> \n                                  <div id=\"order_detail\">\n                                      <p>Size: <span>Small Regular</span></p>\n                                  </div>\n                              </div>\n\n                              <div class=\"col-sm-4\"> \n                                  <div id=\"order_detail\">\n                                      <p>Qty: <span>1 item(s)</span></p>\n                                  </div>\n                              </div>\n                             \n                           </div>\n                             \n                           <div class=\"hidden-sm-up price_tk\">\n                               <p><b>N12,000</b></p>\n                            </div>\n\n                          \n                                  \n                           \n                        </div>\n                       <div class=\"col-sm-4  hidden-xs-down\">\n                           <div class=\"item_order price_tk\">\n                             <p><b>N12,000</b></p>\n                           </div>\n\n                       </div>\n                     </div>\n\n\n                     <!-- First Order Detail -->\n                    <br>\n                    <div class=\"row\">\n                     <div class=\"col-sm-2\">    \n                      </div>\n                       <div class=\"col-sm-6 \">\n                         <div class=\"item_order\">\n                            <p><b>Print ethno tank</b></p>\n                         </div>\n                          <!--  -->\n                           <div class=\"row\">\n                               <div class=\"col order_number\">\n                                 <p>#1232323243</p>\n                               </div>\n                            </div>\n                           <div class=\"row\">\n                              <div class=\"col-sm-4\"> \n                                  <div id=\"order_detail\">\n                                     <p>Color: <span>Black</span></p>\n                                  </div>\n                              </div>\n\n                              <div class=\"col-sm-4\"> \n                                  <div id=\"order_detail\">\n                                      <p>Size: <span>Small Regular</span></p>\n                                  </div>\n                              </div>\n\n                              <div class=\"col-sm-4\"> \n                                  <div id=\"order_detail\">\n                                      <p>Qty: <span>1 item(s)</span></p>\n                                  </div>\n                              </div>\n                             \n                           </div>\n                             \n                           <div class=\"hidden-sm-up price_tk\">\n                               <p><b>N12,000</b></p>\n                            </div>\n\n                          \n                                  \n                           \n                        </div>\n                       <div class=\"col-sm-4  hidden-xs-down\">\n                           <div class=\"item_order price_tk\">\n                             <p><b>N12,000</b></p>\n                           </div>\n\n                       </div>\n                     </div>\n\n\n                     <!-- First Order Detail -->\n                    <br>\n                    <div class=\"row\">\n                     <div class=\"col-sm-2\">    \n                      </div>\n                       <div class=\"col-sm-6 \">\n                         <div class=\"item_order\">\n                            <p><b>Print ethno tank</b></p>\n                         </div>\n                          <!--  -->\n                           <div class=\"row\">\n                               <div class=\"col order_number\">\n                                 <p>#1232323243</p>\n                               </div>\n                            </div>\n                           <div class=\"row\">\n                              <div class=\"col-sm-4\"> \n                                  <div id=\"order_detail\">\n                                     <p>Color: <span>Black</span></p>\n                                  </div>\n                              </div>\n\n                              <div class=\"col-sm-4\"> \n                                  <div id=\"order_detail\">\n                                      <p>Size: <span>Small Regular</span></p>\n                                  </div>\n                              </div>\n\n                              <div class=\"col-sm-4\"> \n                                  <div id=\"order_detail\">\n                                      <p>Qty: <span>1 item(s)</span></p>\n                                  </div>\n                              </div>\n                             \n                           </div>\n                             \n                           <div class=\"hidden-sm-up price_tk\">\n                               <p><b>N12,000</b></p>\n                            </div>\n\n                          \n                                  \n                           \n                        </div>\n                       <div class=\"col-sm-4  hidden-xs-down\">\n                           <div class=\"item_order price_tk\">\n                             <p><b>N12,000</b></p>\n                           </div>\n\n                       </div>\n                     </div>\n\n                  </div>\n                  \n                  <br>\n                  <!-- ******************************************************************\n                      Order Details \n                   *************************************************************-->\n                 <div class=\"row\"> \n                       <div class=\"col-sm-2\">\n                         <h5><b>ESTIMATION</b></h5>\n                       </div>\n                  </div>\n\n                  <div class=\"row\">\n                     <div class=\"col-sm-2\">    \n                      </div>\n                       <div class=\"col-sm-5 up_padding_tk\">\n                         <div class=\"item_order\">\n                            <p>Order</p>\n                         </div>\n                         <div class=\"hidden-sm-up price_tk\">\n                             <p><b>N12,000</b></p>\n                          </div>\n\n                          \n                          <div class=\"item_shipping\">\n                             <p>Shipping</p>\n                          </div>\n                          <div class=\"hidden-sm-up price_tk\">\n                             <p><b>N1,000</b></p>\n                           </div>\n\n                          \n                          <div class=\"item_bonus\">\n                             <p>Bonus / Discount</p> \n                          </div> \n                          <div class=\"hidden-sm-up price_tk\">\n                             <p><b>N2,000</b></p>\n                           </div>\n\n                           \n                          <div class=\"item_taxes\">\n                             <p>Taxes</p> \n                          </div> \n                          <div class=\"hidden-sm-up price_tk\">\n                             <p><b>N0</b></p>\n                           </div> \n\n\n                          <div class=\"item_total\">\n                             <p>Total</p> \n                          </div> \n                          <div class=\"hidden-sm-up price_tk\">\n                             <p><b>N15,000</b></p>\n                           </div>               \n                           \n                       </div>\n                       <div class=\"col-sm-5 up_padding_tk hidden-xs-down\">\n                           <div class=\"item_order price_tk\">\n                             <p><b>N12,000</b></p>\n                           </div>\n\n                          \n                           <div class=\"item_shipping price_tk\">\n                             <p><b>N1,000</b></p>\n                           </div>\n\n                           \n                           <div class=\"item_bonus price_tk\">\n                             <p><b>N2,000</b></p>\n                           </div>\n\n                           \n                           <div class=\"item_taxes price_tk\">\n                             <p><b>N0</b></p>\n                           </div>\n\n                           <div class=\"item_total price_tk\">\n                             <p><b>N15,000</b></p>\n                           </div>\n\n                       </div>\n                     </div>\n\n                  <br>\n                  <!--****************************************\n                        Customer Information\n                    ********************************************* -->\n                 <div class=\"row\"> \n                       <div class=\"col\">\n                         <h5><b>CUSTOMER INFORMATION</b></h5>\n                       </div>\n                  </div>\n\n                  <div class=\"row\">\n                     <div class=\"col-sm-2\">    \n                      </div>\n                       <div class=\"col-sm-5 up_padding_tk\">\n                           <h6><b>Shipping Address</b></h6>\n                           <br>\n\n                           <div class=\"ship_name\">\n                              <p>Dantex Lex</p>\n                           </div>\n                                                     \n                           <div class=\"ship_adr\">\n                               <p>23 Pavlon Dr, V.I</p>\n                           </div>\n                            \n                          <div class=\"item_\">\n                               <p>Lagos, 23401 </p> \n                          </div> \n                             \n                          <div class=\"item_\">\n                               <p>080234534XX</p> \n                          </div> \n                                           \n                       </div>\n\n                       <div class=\"col-sm-5 up_padding_tk\">\n                           \n                           <h6><b>Billing Address</b></h6>\n                           <br>\n\n                           <div class=\"ship_name\">\n                              <p>Dantex Lex</p>\n                           </div>\n                                                     \n                           <div class=\"ship_adr\">\n                               <p>23 Pavlon Dr, V.I</p>\n                           </div>\n                            \n                          <div class=\"item_\">\n                               <p>Lagos, 23401 </p> \n                          </div> \n                             \n                          <div class=\"item_\">\n                               <p>080234534XX</p> \n                          </div> \n                                   \n                       </div>\n                     </div>\n\n                     <!--  -->\n                     <div class=\"row\">\n                     <div class=\"col-sm-2\">    \n                      </div>\n                       <div class=\"col-sm-5 up_padding_tk\">\n                           <h6><b>Shipping Method</b></h6>\n                           <br>\n\n                           <div class=\"ship_name\">\n                              <p> Standard 3-5 business days</p>\n                           </div>\n                                                     \n                          \n                                           \n                       </div>\n\n                       <div class=\"col-sm-5 up_padding_tk\">\n                           \n                           <h6><b>Payment Method</b></h6>\n                           <br>\n                  \n                           \n                           \n                           <div class=\"row\">\n                             <div class=\"col-6\">\n                               <div class=\"ship_name\">\n                                  <p>Credit Card</p>\n                               </div>\n                             </div>\n                             <div class=\"col-6\">\n                                <div class=\"ship_name\">\n                                  <p>1232******45</p>\n                               </div>\n                             </div>\n\n                           </div>                          \n                         \n                                   \n                       </div>\n                     </div>\n             </section>\n            </div>\n          </div>\n        </div>\n        \n        \n\n      </div> \n    <br>\n   </div>\n"

/***/ }),

/***/ "./src/app/order-confirmation/order-confirmation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderConfirmationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OrderConfirmationComponent = (function () {
    function OrderConfirmationComponent() {
    }
    OrderConfirmationComponent.prototype.ngOnInit = function () {
    };
    OrderConfirmationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-order-confirmation',
            template: __webpack_require__("./src/app/order-confirmation/order-confirmation.component.html"),
            styles: [__webpack_require__("./src/app/order-confirmation/order-confirmation.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], OrderConfirmationComponent);
    return OrderConfirmationComponent;
}());



/***/ }),

/***/ "./src/app/order-confirmation/order-confirmation.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderConfirmationModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__order_confirmation_component__ = __webpack_require__("./src/app/order-confirmation/order-confirmation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared__ = __webpack_require__("./src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var orderConfirmationRouting = __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild([
    {
        path: 'order-confirmation',
        component: __WEBPACK_IMPORTED_MODULE_2__order_confirmation_component__["a" /* OrderConfirmationComponent */]
    }
]);
var OrderConfirmationModule = (function () {
    function OrderConfirmationModule() {
    }
    OrderConfirmationModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                orderConfirmationRouting,
                __WEBPACK_IMPORTED_MODULE_3__shared__["a" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__order_confirmation_component__["a" /* OrderConfirmationComponent */]
            ],
            providers: []
        })
    ], OrderConfirmationModule);
    return OrderConfirmationModule;
}());



/***/ }),

/***/ "./src/app/product/category/category.component.css":
/***/ (function(module, exports) {

module.exports = "\r\n#content_wrapper {\r\n  background:  #f1f2f2;\r\n}\r\n\r\n"

/***/ }),

/***/ "./src/app/product/category/category.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<!-- ============ Search Results ============ -->\r\n   <div id=\"content_wrapper\">\r\n       <div class=\"container-fluid\">\r\n         <div class=\"row\">\r\n          <!-- search sidebar -->\r\n            <div class=\"col-md-3 hidden-md-down\" style=\"padding-right: 5px;\">\r\n              <div class=\"s_sidebar\">\r\n                <div class=\"filters\">\r\n                  <h5>Filters</h5> \r\n\r\n                </div>\r\n                <hr>\r\n\r\n                <div class=\"category\" >\r\n                  <h5>Categories</h5>\r\n                   <ul class=\"list-unstyled components\">\r\n                    <li class=\"\">\r\n                        <a href=\"#\" ><i class=\"fa fa-chevron-left\"></i> {{catname}}</a>\r\n                     </li>\r\n                  \r\n                   </ul>\r\n                </div>\r\n                <hr>\r\n\r\n                <div class=\"price\">\r\n                  <h5>Price</h5>\r\n                   <ul class=\"list-unstyled components\">\r\n                    <li class=\"\">\r\n                        <a href=\"#\" > Under N5,000</a>\r\n                     </li>\r\n                     <li class=\"\">\r\n                        <a href=\"#\" > Between 5,000 and N10,000</a>\r\n                     </li>\r\n                     <li class=\"\">\r\n                        <a href=\"#\" > Between 10,000 and N30,000</a>\r\n                     </li>\r\n                     <li class=\"\">\r\n                        <a href=\"#\" > Between 30,000 and N60,000</a>\r\n                     </li>\r\n                     <li class=\"\">\r\n                        <a href=\"#\" > N60,000 and above</a>\r\n                     </li>\r\n                   </ul>\r\n                   <div>\r\n                     <form class=\"form-inline\">\r\n                      <label class=\"sr-only\" for=\"inlineFormInput\">N-min</label>\r\n                      <input type=\"text\" class=\"form-control m_form\" id=\"inlineFormInput\" placeholder=\"Min\">\r\n\r\n                       <label class=\"sr-only\" for=\"inlineFormInput2\">N-max</label>\r\n                      <input type=\"text\" class=\"form-control m_form\" id=\"inlineFormInput2\" placeholder=\"Max\">\r\n\r\n                      <button type=\"submit\" class=\"btn btn-primary blue\">Go</button>\r\n                    </form>\r\n                   </div>\r\n                </div>\r\n                <hr>\r\n\r\n                <div class=\"shipping\">\r\n                  <h5>Shipping</h5> \r\n                  <div class=\"form-check\">\r\n                    <label class=\"form-check-label\">\r\n                      <input type=\"checkbox\" class=\"form-check-input\">\r\n                      Free\r\n                    </label>\r\n                  </div>\r\n                  \r\n                  <div class=\"form-check\">\r\n                      <label class=\"form-check-label\">\r\n                        <input type=\"checkbox\" class=\"form-check-input\">\r\n                        International\r\n                      </label>\r\n                  </div>\r\n\r\n                </div>\r\n                <hr>\r\n                \r\n\r\n                <div class=\"brands\">\r\n                  <h5>Brands</h5> \r\n                  <div class=\"form-check\">\r\n                    <label class=\"form-check-label\">\r\n                      <input type=\"checkbox\" class=\"form-check-input\">\r\n                      Timberland\r\n                    </label>\r\n                  </div>\r\n                  \r\n                  <div class=\"form-check\">\r\n                      <label class=\"form-check-label\">\r\n                        <input type=\"checkbox\" class=\"form-check-input\">\r\n                        Addidas\r\n                      </label>\r\n                  </div>\r\n\r\n                  <div class=\"form-check\">\r\n                    <label class=\"form-check-label\">\r\n                      <input type=\"checkbox\" class=\"form-check-input\">\r\n                      Timberland\r\n                    </label>\r\n                  </div>\r\n\r\n                  <a href=\"#\"><strong>234 more</strong></a>\r\n                </div>\r\n                <hr>\r\n\r\n               <div class=\"features\">\r\n                  <h5>Features</h5> \r\n                  <div class=\"form-check\">\r\n                    <label class=\"form-check-label\">\r\n                      <input type=\"checkbox\" class=\"form-check-input\">\r\n                      Short Sleeves\r\n                    </label>\r\n                  </div>\r\n                  \r\n                  <div class=\"form-check\">\r\n                      <label class=\"form-check-label\">\r\n                        <input type=\"checkbox\" class=\"form-check-input\">\r\n                        Patterned\r\n                      </label>\r\n                  </div>\r\n\r\n                </div>\r\n                <hr>\r\n\r\n                <div class=\"discount\">\r\n                  <h5>Discount</h5> \r\n                  <div class=\"form-check\">\r\n                    <label class=\"form-check-label\">\r\n                      <input type=\"checkbox\" class=\"form-check-input\">\r\n                      50% or more\r\n                    </label>\r\n                  </div>\r\n                  \r\n                  <div class=\"form-check\">\r\n                      <label class=\"form-check-label\">\r\n                        <input type=\"checkbox\" class=\"form-check-input\">\r\n                        20% or more\r\n                      </label>\r\n                  </div>\r\n\r\n                </div>\r\n                <hr>\r\n\r\n                <div class=\"sellers\">\r\n                  <h5>Sellers</h5> \r\n                  <div class=\"form-check\">\r\n                    <label class=\"form-check-label\">\r\n                      <input type=\"checkbox\" class=\"form-check-input\">\r\n                      Amazon\r\n                    </label>\r\n                  </div>\r\n                  \r\n                  <div class=\"form-check\">\r\n                      <label class=\"form-check-label\">\r\n                        <input type=\"checkbox\" class=\"form-check-input\">\r\n                        Multiskills\r\n                      </label>\r\n                  </div>\r\n\r\n                </div>\r\n                <hr>\r\n\r\n                <div class=\"availability\">\r\n                  <h5>Availability</h5> \r\n                  <div class=\"form-check\">\r\n                    <label class=\"form-check-label\">\r\n                      <input type=\"checkbox\" class=\"form-check-input\">\r\n                      Include Out of Stock\r\n                    </label>\r\n                  </div>\r\n                  \r\n                  <div class=\"form-check\">\r\n                      <label class=\"form-check-label\">\r\n                        <input type=\"checkbox\" class=\"form-check-input\">\r\n                        Limited\r\n                      </label>\r\n                  </div>\r\n\r\n                </div>\r\n                <hr>\r\n\r\n                <div class=\"colors\">\r\n                  <h5>Colors</h5> \r\n                  <div class=\"form-check\">\r\n                    <label class=\"form-check-label\">\r\n                      <input type=\"checkbox\" class=\"form-check-input\">\r\n                      Green\r\n                    </label>\r\n                  </div>\r\n                  \r\n                  <div class=\"form-check\">\r\n                      <label class=\"form-check-label\">\r\n                        <input type=\"checkbox\" class=\"form-check-input\">\r\n                        Blue\r\n                      </label>\r\n                  </div>\r\n\r\n                </div>\r\n                <hr>\r\n\r\n                <div class=\"offers\">\r\n                  <h5>Offers</h5> \r\n                  <div class=\"form-check\">\r\n                    <label class=\"form-check-label\">\r\n                      <input type=\"checkbox\" class=\"form-check-input\">\r\n                      Bank Offer\r\n                    </label>\r\n                  </div>\r\n                  \r\n                  <div class=\"form-check\">\r\n                      <label class=\"form-check-label\">\r\n                        <input type=\"checkbox\" class=\"form-check-input\">\r\n                        Special Offer\r\n                      </label>\r\n                  </div>\r\n\r\n                </div>\r\n                <hr>\r\n\r\n                <div class=\"occasion\">\r\n                  <h5>Occasion</h5> \r\n                  <div class=\"form-check\">\r\n                    <label class=\"form-check-label\">\r\n                      <input type=\"checkbox\" class=\"form-check-input\">\r\n                      Beach Wear\r\n                    </label>\r\n                  </div>\r\n                  \r\n                  <div class=\"form-check\">\r\n                      <label class=\"form-check-label\">\r\n                        <input type=\"checkbox\" class=\"form-check-input\">\r\n                        Sport\r\n                      </label>\r\n                  </div>\r\n\r\n                </div>\r\n                \r\n\r\n              </div>\r\n            </div>\r\n            <!-- Modal for the filter on mobile view -->\r\n            <div class=\"col hidden-lg-up fil\">\r\n                \r\n                <h6 class=\"float-right filter-title\" data-toggle=\"modal\" data-target=\"#modal1\"><img src=\"webfonts/filter.svg\" width=\"20px\"> Filters </h6>\r\n              \r\n            </div>\r\n            <!-- Modal -->\r\n            <div class=\"modal \" id=\"modal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\" aria-hidden=\"true\">\r\n              <div class=\"modal-dialog-fil\" role=\"document\">\r\n                <div class=\"modal-content-fil modal-content-one\">\r\n                  <div class=\"modal-header\">\r\n                    <h4 class=\"modal-title\" id=\"exampleModalLongTitle\">Filters</h4>\r\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                   <span aria-hidden=\"true\">&times;</span>\r\n            </button>\r\n                    \r\n                  </div>\r\n                  <div class=\"modal-body\">\r\n                    <div class=\"container\">\r\n                      <div class=\"row\">\r\n                        <!--  -->\r\n                        \r\n                        <!--  -->\r\n                        <div class=\"col-md-6\">\r\n                          <div class=\"price\">\r\n                          <h5>Price</h5>\r\n                           <ul class=\"list-unstyled components\">\r\n                            <li class=\"\">\r\n                                <a href=\"#\" > Under N5,000</a>\r\n                             </li>\r\n                             <li class=\"\">\r\n                                <a href=\"#\" > Between 5,000 and N10,000</a>\r\n                             </li>\r\n                             <li class=\"\">\r\n                                <a href=\"#\" > Between 10,000 and N30,000</a>\r\n                             </li>\r\n                             <li class=\"\">\r\n                                <a href=\"#\" > Between 30,000 and N60,000</a>\r\n                             </li>\r\n                             <li class=\"\">\r\n                                <a href=\"#\" > N60,000 and above</a>\r\n                             </li>\r\n                           </ul>\r\n                           <div>\r\n                             <form class=\"form-inline\">\r\n                              <label class=\"sr-only\" for=\"inlineFormInput3\">N-min</label>\r\n                              <input type=\"text\" class=\"form-control m_form\" id=\"inlineFormInput3\" placeholder=\"Min\">\r\n\r\n                               <label class=\"sr-only\" for=\"inlineFormInput4\">N-max</label>\r\n                              <input type=\"text\" class=\"form-control m_form\" id=\"inlineFormInput4\" placeholder=\"Max\">\r\n\r\n                              <button type=\"submit\" class=\"btn btn-primary blue\">Go</button>\r\n                            </form>\r\n                           </div>\r\n                        </div>\r\n                        <hr>\r\n                        </div>\r\n                        <!--  -->\r\n                        <div class=\"col-md-6\">\r\n                           <div class=\"shipping\">\r\n                            <h5>Shipping</h5> \r\n                            <div class=\"form-check\">\r\n                              <label class=\"form-check-label\">\r\n                                <input type=\"checkbox\" class=\"form-check-input\">\r\n                                Free\r\n                              </label>\r\n                            </div>\r\n                            \r\n                            <div class=\"form-check\">\r\n                                <label class=\"form-check-label\">\r\n                                  <input type=\"checkbox\" class=\"form-check-input\">\r\n                                  International\r\n                                </label>\r\n                            </div>\r\n\r\n                          </div>\r\n                          <hr>\r\n                        </div>\r\n                        <!--  -->\r\n                        <div class=\"col-md-6\">\r\n                          <div class=\"brands\">\r\n                            <h5>Brands</h5> \r\n                            <div class=\"form-check\">\r\n                              <label class=\"form-check-label\">\r\n                                <input type=\"checkbox\" class=\"form-check-input\">\r\n                                Timberland\r\n                              </label>\r\n                            </div>\r\n                            \r\n                            <div class=\"form-check\">\r\n                                <label class=\"form-check-label\">\r\n                                  <input type=\"checkbox\" class=\"form-check-input\">\r\n                                  Addidas\r\n                                </label>\r\n                            </div>\r\n\r\n                            <div class=\"form-check\">\r\n                              <label class=\"form-check-label\">\r\n                                <input type=\"checkbox\" class=\"form-check-input\">\r\n                                Timberland\r\n                              </label>\r\n                            </div>\r\n\r\n                            <a href=\"#\"><strong>234 more</strong></a>\r\n                          </div>\r\n                          <hr>\r\n                        </div>\r\n                         <!--  -->\r\n                         <div class=\"col-md-6\">\r\n                          <div class=\"features\">\r\n                            <h5>Features</h5> \r\n                            <div class=\"form-check\">\r\n                              <label class=\"form-check-label\">\r\n                                <input type=\"checkbox\" class=\"form-check-input\">\r\n                                Short Sleeves\r\n                              </label>\r\n                            </div>\r\n                            \r\n                            <div class=\"form-check\">\r\n                                <label class=\"form-check-label\">\r\n                                  <input type=\"checkbox\" class=\"form-check-input\">\r\n                                  Patterned\r\n                                </label>\r\n                            </div>\r\n\r\n                          </div>\r\n                          <hr>\r\n                        </div>\r\n                         <!--  -->\r\n                         <div class=\"col-md-6\">\r\n                          <div class=\"discount\">\r\n                          <h5>Discount</h5> \r\n                            <div class=\"form-check\">\r\n                              <label class=\"form-check-label\">\r\n                                <input type=\"checkbox\" class=\"form-check-input\">\r\n                                50% or more\r\n                              </label>\r\n                            </div>\r\n                            \r\n                            <div class=\"form-check\">\r\n                                <label class=\"form-check-label\">\r\n                                  <input type=\"checkbox\" class=\"form-check-input\">\r\n                                  20% or more\r\n                                </label>\r\n                            </div>\r\n\r\n                          </div>\r\n                          <hr>\r\n                        </div>\r\n                         <!--  -->\r\n                         <div class=\"col-md-6\">\r\n                          <div class=\"sellers\">\r\n                          <h5>Sellers</h5> \r\n                            <div class=\"form-check\">\r\n                              <label class=\"form-check-label\">\r\n                                <input type=\"checkbox\" class=\"form-check-input\">\r\n                                Amazon\r\n                              </label>\r\n                            </div>\r\n                            \r\n                            <div class=\"form-check\">\r\n                                <label class=\"form-check-label\">\r\n                                  <input type=\"checkbox\" class=\"form-check-input\">\r\n                                  Multiskills\r\n                                </label>\r\n                            </div>\r\n\r\n                          </div>\r\n                          <hr>\r\n                        </div>\r\n                         <!--  -->\r\n                         <div class=\"col-md-6\">\r\n                          <h5>Availability</h5>\r\n                            <h5>Availability</h5> \r\n                            <div class=\"form-check\">\r\n                              <label class=\"form-check-label\">\r\n                                <input type=\"checkbox\" class=\"form-check-input\">\r\n                                Include Out of Stock\r\n                              </label>\r\n                            </div>\r\n                            \r\n                            <div class=\"form-check\">\r\n                                <label class=\"form-check-label\">\r\n                                  <input type=\"checkbox\" class=\"form-check-input\">\r\n                                  Limited\r\n                                </label>\r\n                            </div>\r\n\r\n                          </div>\r\n                          <hr>\r\n                        </div>\r\n                         <!--  -->\r\n                       </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              \r\n          \r\n\r\n            <!-- search main -->\r\n            <div class=\"col-md-12 col-lg-9\" style=\"padding-left: 5px;\">\r\n              <div class=\" s_main\">\r\n                <div class=\"col\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col bread_crumb\">\r\n                      <ol class=\"breadcrumb\">\r\n                        <li class=\"breadcrumb-item\"><a href=\"#\">Home</a></li>\r\n                        <li class=\"breadcrumb-item\"><a href=\"#\">Clothing</a></li>\r\n                        <li class=\"breadcrumb-item\">Men's Clothing</li>\r\n                        <li class=\"breadcrumb-item active\">T-shirts</li>\r\n                      </ol>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"row\">\r\n                    <div class=\"col-sm-8\">\r\n                      <div id=\"search_result\">\r\n                        <h4>{{catname}} </h4>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"col-sm-4\">\r\n                      <div id=\"sort_by\" class=\"float-right\">\r\n                        <div class=\"dropdown\">\r\n                          Sort by <a class=\"btn btn-secondary dropdown-toggle\" href=\"https://example.com\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n                            Featured\r\n                          </a>\r\n\r\n                          <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">\r\n                            <a class=\"dropdown-item\" href=\"#\">Price</a>\r\n                            <a class=\"dropdown-item\" href=\"#\">Size</a>\r\n                            <a class=\"dropdown-item\" href=\"#\">Brand</a>\r\n                          </div>\r\n                          </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                 <hr>\r\n\r\n\r\n                <div class=\"col\">\r\n                  <div class=\"row\"> <!-- first row-->\r\n                    <div class=\"col-md-3\"   *ngFor=\"let product of productItems\">\r\n                      \r\n                        <!-- Card  -->\r\n                        <div class=\"card\">\r\n                          <a href=\"{{product.url}}\">\r\n                          <img class=\"card-img-top\" src=\"{{product.image_path}}\" alt=\"{{product.name}}\"></a>\r\n                          <div class=\"card-block\">\r\n                            <h6 class=\"card-title text-left\">{{product.name}}</h6>\r\n                            <section id=\"rating\" class=\"text-left\">\r\n                              \r\n                              <p><i class=\"fa fa-star-half-full\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i></p>\r\n\r\n                              <p id=\"reviews\">{{product.reviewCount}}</p>\r\n                            </section>\r\n                            <h6 id=\"price\" class=\"text-left\">  {{getCurrency()}}{{product.price | number : '1.2-2'}}</h6>\r\n                            <button class=\"btn btn-primary blue\" href=\"#\">Add to Cart</button>\r\n                         </div>    \r\n                       </div>\r\n\r\n                    </div>\r\n\r\n                  \r\n\r\n                  </div> \r\n              </div>\r\n            </div>\r\n         </div>\r\n       </div>\r\n       <br>\r\n   </div><!-- End of content wrapper -->\r\n\r\n        <script src=\"assets/js/ie10-viewport-bug-workaround.js\"></script>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/product/category/category.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_owl_carousel_dist_owl_carousel_min__ = __webpack_require__("./node_modules/owl.carousel/dist/owl.carousel.min.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_owl_carousel_dist_owl_carousel_min___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_owl_carousel_dist_owl_carousel_min__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_Serv_service__ = __webpack_require__("./src/app/services/Serv.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CategoryComponent = (function () {
    function CategoryComponent(categoryService, route) {
        var _this = this;
        this.categoryService = categoryService;
        this.route = route;
        this.productItems = [];
        this.topselling_phones = [];
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            // Category Service
            _this.categoryService.getAnyCategoryProducts(_this.id).subscribe(function (response) {
                var products = response.json();
                for (var _i = 0, products_1 = products; _i < products_1.length; _i++) {
                    var product = products_1[_i];
                    _this.productItems.push(product);
                }
            });
        });
        this.topselling_phones = [
            { name: 'assets/images/demo1.jpg' },
            { name: 'assets/images/demo1.jpg' },
            { name: 'assets/images/demo1.jpg' },
            { name: 'assets/images/demo1.jpg' },
        ];
    }
    CategoryComponent.prototype.getCurrency = function () {
        return '₦';
    };
    CategoryComponent.prototype.ngOnInit = function () {
        jQuery('.owl-carousel').owlCarousel(jQuery('.owl-carousel').owlCarousel({
            rtl: true,
            loop: true,
            margin: 10,
            nav: true,
            autoplay: false,
            dots: false,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                800: {
                    items: 4
                },
                1000: {
                    items: 4
                }
            }
        }));
    };
    CategoryComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-category',
            template: __webpack_require__("./src/app/product/category/category.component.html"),
            styles: [__webpack_require__("./src/app/product/category/category.component.css")],
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["j" /* ChangeDetectionStrategy */].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_Serv_service__["a" /* CategoryService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */]])
    ], CategoryComponent);
    return CategoryComponent;
}());



/***/ }),

/***/ "./src/app/product/category/category.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__category_component__ = __webpack_require__("./src/app/product/category/category.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared__ = __webpack_require__("./src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var categoryRouting = __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild([
    {
        path: 'category/:id',
        component: __WEBPACK_IMPORTED_MODULE_2__category_component__["a" /* CategoryComponent */]
    }
]);
var CategoryModule = (function () {
    function CategoryModule() {
    }
    CategoryModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                categoryRouting,
                __WEBPACK_IMPORTED_MODULE_3__shared__["a" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__category_component__["a" /* CategoryComponent */]
            ],
            providers: []
        })
    ], CategoryModule);
    return CategoryModule;
}());



/***/ }),

/***/ "./src/app/product/productpage/productpage.component.css":
/***/ (function(module, exports) {

module.exports = "/*------------------------------------------\r\n  Product Page\r\n------------------------------------------------*/\r\n.product {\r\n  background: #fff;\r\n  padding: 20px 0px 20px 0px;\r\n\r\n}\r\n.product p {\r\n  color: #343434;\r\n  font-size: 16px;\r\n  font-weight: 100;\r\n  margin-bottom: 10px;\r\n}\r\n.more_padding {\r\n  padding-top: 20px;\r\n}\r\n.product h4 {\r\n   color: rgba(0, 0, 0, 0.54);\r\n}\r\n.product h5{\r\n  font-weight: bold;\r\n}\r\n.action_btn_p{\r\n   margin-top: 20px;\r\n   margin-bottom: 10px;\r\n}\r\n.new_blue {\r\n    background: #fff;\r\n    border-color: #18b8e5;\r\n    padding: 10px 30px;\r\n    color: #18b8e5;\r\n    font-weight: bold;\r\n}\r\n.new_blue:hover {\r\n    background: #18b8e5;\r\n    border-color: #18b8e5;\r\n    padding: 10px 30px;\r\n    color: #fff;\r\n    font-weight: bold;\r\n}\r\n.blue_cart{\r\n    background: #18b8e5;\r\n    border-color: #18b8e5;\r\n    padding: 10px 30px;\r\n    color: #fff;\r\n    font-weight: bold;\r\n    margin-left: 10px;\r\n}\r\n.blue_cart:hover{\r\n    background: #18b8e5;\r\n    border-color: #18b8e5;\r\n    padding: 10px 30px;\r\n    color: #fff;\r\n    font-weight: bold;\r\n}\r\n.new_bread_crumb {\r\n  font-size: 10px;\r\n  margin-bottom: 10px;\r\n}\r\n#color_balls {\r\n  display: -webkit-inline-box;\r\n  display: -ms-inline-flexbox;\r\n  display: inline-flex;\r\n  -webkit-box-sizing: border-box;\r\n          box-sizing: border-box;\r\n  padding-top: 10px;\r\n}\r\n#color_balls p{\r\n  margin-right: 10px;\r\n}\r\n.col_dark {\r\n  color: #343434;\r\n}\r\n.col_blue {\r\n  color: #18b8e5;\r\n}\r\n.col_yellow {\r\n  color:#fec04e;\r\n}\r\n.col_pink {\r\n  color: #fe4e4e;\r\n}\r\n.product_desc a{\r\n  color: #fe4e4e;\r\n}\r\n#product_r i {\r\n  font-size: 10px;\r\n}\r\n#rating span {\r\n  font-size: 10px;\r\n  font-weight: bold;\r\n  color: rgba(0, 0, 0, 0.54);\r\n}\r\n.product_image {\r\n \r\n\r\n}\r\n#product_p {\r\n  font-size: 10px;\r\n}\r\n.productcard-image {\r\n  border-radius: 10px;\r\n  margin: 0 auto;\r\n}\r\n#palette-image li{\r\n  list-style-type: none;\r\n  display: -webkit-inline-box;\r\n  display: -ms-inline-flexbox;\r\n  display: inline-flex;\r\n  margin-right: 10px;\r\n  border-radius: 10px; \r\n  width: 60px;\r\n  height: 50px;\r\n}\r\n#palette-image{\r\n  margin: 0 auto;\r\n  padding-top: 15px;\r\n}"

/***/ }),

/***/ "./src/app/product/productpage/productpage.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- ============ Product ============ -->\n   <div id=\"content_wrapper\">\n     <br>\n     <div class=\"container-fluid \">\n       <div class=\"row product\">\n         <div class=\"col-sm-5\">\n           <div class=\"product_image\">\n             <div class=\"card\">\n               <div class=\"productcard-image\">\n                 <img id=\"pc_img\" src=\"{{product.image_path}}\" alt=\"{{product.name}}\" class=\"img-fluid\">\n               </div>\n               <div id=\"palette-image\">\n                <ul>\n                  <li><img id=\"p_img\" src=\"{{product.small_image}}\" class=\"img-fluid\"></li>\n\n               </ul>\n               </div>\n             </div>\n           </div>\n         </div>\n\n         <div class=\"col-sm-7\">\n          <div class=\"product_details\">\n          <!-- Breadcrumb -->\n            <div class=\"bread_crumb \">\n              <ol class=\"breadcrumb new_bread_crumb\">\n                <li class=\"breadcrumb-item\"><a href=\"#\">Home</a></li>\n                <li class=\"breadcrumb-item\"><a href=\"#\">Men's Clothing</a></li>\n                <li class=\"breadcrumb-item active\">{{product.name}}</li>\n              </ol>\n            </div>\n\n            <!--  -->\n            <div id=\"rating\"> \n              <h3 class=\"cart_product_title\">{{product.name}}</h3>                 \n              <p id=\"product_r\">\n                <i class=\"fa fa-star-half-full\"></i>\n                <i class=\"fa fa-star\"></i>\n                <i class=\"fa fa-star\"></i>\n                <i class=\"fa fa-star\"></i>\n                <i class=\"fa fa-star\"></i>\n              </p>\n\n              <span id=\"product_p\">{{product.reviewCount}}</span>\n\n              <h3>  {{getCurrency()}}{{product.price | number : '1.2-2'}}</h3>      \n            </div>\n\n            <!-- Action Buttons -->\n            <div class=\"action_btn_p\">\n              <a href=\"#\" class=\"btn btn-primary new_blue\">Add to Cart</a>\n              <a href=\"#\" class=\"btn btn-primary blue_cart\">Buy it now</a>\n              <hr>\n            </div>\n\n            <!-- Offers -->\n            <div>\n              <h4>OFFERS</h4>\n              <p>Special Price Get N15,000 off (price inckusive of discount)</p>\n              <p>Bank Offer Extra 5% off with Axis Bank Buzz Credit Card</p>\n              <p>Partner Offer First Purchase on mobiles, Get Extra 10% off on your next purchase on slect fashion products</p>\n            </div>\n\n            <!-- Offer - full -->\n            <div class=\"row more_padding\">\n               <div class=\"col-sm-6\">\n                  <h4>WARRANTY</h4>\n                  <p>6 Months Warranty</p>\n               </div>\n               <div class=\"col-sm-6\">\n                  <h4>PAYMENT OPTIONS</h4>\n                  <p>Cash On Delivery</p>\n                  <p>Net banking &amp; Credit/ Debit/ ATM card</p>\n               </div>\n            </div>\n            \n            <!-- Offer - full -->\n            <div class=\"row more_padding\">\n               \n               <div class=\"col-sm-6\" *ngIf=\"product.type_id==='configurable'\">\n                  <h4>COLOR</h4>\n                  <div id=\"color_balls\">\n                    <p><i class=\"fa fa-circle fa-2x col_dark\"></i></p>\n                    <p><i class=\"fa fa-circle fa-2x col_blue\"></i></p>\n                    <p><i class=\"fa fa-circle fa-2x col_yellow\"></i></p>\n                    <p><i class=\"fa fa-circle fa-2x col_pink\"></i></p>\n\n                  </div>\n\n               </div>\n               <div class=\"col-sm-6\">\n                  <h4>DELIVERY</h4>\n                  <p>Usually delivered in 3-5 business days</p>\n               </div>\n            </div>\n\n\n            <!-- Offer - full -->\n            <div class=\"row more_padding\">\n               <div class=\"col-sm-6\" *ngIf=\"product.type_id==='configurable'\">\n                  <h4>SIZE</h4>\n                  <form>\n                    \n                      <select class=\"form-control\">\n                        <option value=\"XS\">XS</option>\n                        <option value=\"M\">M</option>\n                        <option value=\"XL\">XL</option>\n                        <option value=\"L\">L</option>\n                      </select>\n                 \n                  </form>\n               </div>\n               <div class=\"col-sm-6\">\n                  <h4>HIGHLIGHTS</h4>\n                  <p>Condition: New</p>\n                  <p>Brand: {{product.brand}}</p>\n                  <p>Category: {{product.category}}</p>\n                  <p>Material: Cotton</p>\n               </div>\n            </div>\n\n            <!--  -->\n            <div class=\"product_desc\">\n               <h4>DESCRIPTION</h4>\n               <p  [innerHTML]=\"product.description\">\n               \n               </p>\n            </div>\n\n\n            \n          </div>\n          </div>\n         </div>\n       </div>\n       <br>\n     </div>\n\n     "

/***/ }),

/***/ "./src/app/product/productpage/productpage.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductpageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__productpage_service__ = __webpack_require__("./src/app/product/productpage/productpage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductpageComponent = (function () {
    function ProductpageComponent(productService, route) {
        var _this = this;
        this.productService = productService;
        this.route = route;
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            // Category Service
            _this.productService.getProductDetails(_this.id).subscribe(function (response) {
                var data = response.json();
                var _loop_1 = function (product) {
                    product.custom_attributes.forEach(function (attr) {
                        if (attr.attribute_code === 'small_image') {
                            product.small_image = 'http://magento.amalinze.com/pub/media/catalog/product' + attr.value;
                        }
                        if (attr.attribute_code === 'image') {
                            product.image_path = 'http://magento.amalinze.com/pub/media/catalog/product' + attr.value;
                        }
                        if (attr.attribute_code === 'thumbnail') {
                            product.thumbnail = 'http://magento.amalinze.com/pub/media/catalog/product' + attr.value;
                        }
                        if (attr.attribute_code === 'meta_title') {
                            product.meta_title = attr.value;
                        }
                        if (attr.attribute_code === 'meta_keyword') {
                            product.meta_keyword = attr.value;
                        }
                        if (attr.attribute_code === 'meta_description') {
                            product.meta_description = attr.value;
                        }
                        if (attr.attribute_code === 'description') {
                            product.description = attr.value;
                        }
                        if (attr.attribute_code === 'short_description') {
                            product.short_description = attr.value;
                        }
                    });
                    _this.product = product;
                };
                for (var _i = 0, _a = data.items; _i < _a.length; _i++) {
                    var product = _a[_i];
                    _loop_1(product);
                }
            });
        });
    }
    ProductpageComponent.prototype.getCurrency = function () {
        return '₦';
    };
    ProductpageComponent.prototype.ngOnInit = function () {
        jQuery("#p_img").click(function () {
            var img_src = jQuery("#p_img").attr('src');
            jQuery("#pc_img").attr("src", img_src);
        });
        jQuery("#p_img2").click(function () {
            var img_src = jQuery("#p_img2").attr('src');
            jQuery("#pc_img").attr("src", img_src);
        });
        jQuery("#p_img3").click(function () {
            var img_src = jQuery("#p_img3").attr('src');
            jQuery("#pc_img").attr("src", img_src);
        });
        jQuery("#p_img4").click(function () {
            var img_src = jQuery("#p_img4").attr('src');
            jQuery("#pc_img").attr("src", img_src);
        });
        jQuery("#p_img5").click(function () {
            var img_src = jQuery("#p_img5").attr('src');
            jQuery("#pc_img").attr("src", img_src);
        });
    };
    ProductpageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-productpage',
            template: __webpack_require__("./src/app/product/productpage/productpage.component.html"),
            styles: [__webpack_require__("./src/app/product/productpage/productpage.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__productpage_service__["a" /* ProductpageService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]])
    ], ProductpageComponent);
    return ProductpageComponent;
}());



/***/ }),

/***/ "./src/app/product/productpage/productpage.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductpageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__productpage_component__ = __webpack_require__("./src/app/product/productpage/productpage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared__ = __webpack_require__("./src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ProductpageRouting = __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild([
    {
        path: 'product-detail/:id',
        component: __WEBPACK_IMPORTED_MODULE_2__productpage_component__["a" /* ProductpageComponent */]
    }
]);
var ProductpageModule = (function () {
    function ProductpageModule() {
    }
    ProductpageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                ProductpageRouting,
                __WEBPACK_IMPORTED_MODULE_3__shared__["a" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__productpage_component__["a" /* ProductpageComponent */]
            ],
            providers: []
        })
    ], ProductpageModule);
    return ProductpageModule;
}());



/***/ }),

/***/ "./src/app/product/productpage/productpage.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductpageService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/_esm5/Rx.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProductpageService = (function () {
    function ProductpageService(http) {
        this.http = http;
        this._http = http;
    }
    ProductpageService.prototype.getProductDetails = function (id) {
        return this._http.get('http://magento.amalinze.com/api/getProductDetails.php?id=' + id);
    };
    ProductpageService.prototype._handleError = function (error) {
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(error.json() || 'Server error');
    };
    ProductpageService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], ProductpageService);
    return ProductpageService;
}());



/***/ }),

/***/ "./src/app/profile/address-book/address-book.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/profile/address-book/address-book.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- ============ Your account ============ -->\n<div id=\"content_wrapper\">\n  <br>\n  <div class=\"container-fluid\">\n    <div class=\"row account\">\n      <div class=\"col-md-4 no_padding onset-6\">\n        <div class=\"user_info\">\n          <h3>Cassy Doe</h3>\n          <p>Lagos, Nigeria</p>\n        </div>\n        <div id=\"user_desc\">\n          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n          </p>\n        </div>\n\n      </div>\n    </div>\n\n\n    <!-- Action Tabs  -->\n    <div class=\"row tab_action\">\n      <div class=\"add_button float-right\">\n         <button class=\"btn address_toggle btn-default\">Add New Addresss</button>\n      </div>\n      </div>\n\n      <!-- ******************************************************************\n                      Address \n                   *************************************************************-->\n\n\n      <!--  -->\n      <div class=\"row tab_action new_address a_account\">\n        <div class=\"col-sm-2 \">\n          <h5><b>New Address</b></h5>\n        </div>\n        <div class=\"col-sm-8 up_padding\">\n          <!--  -->\n          <form>\n            <br>\n            <!-- <div class=\"form-group fcl2\">\n                    <label for=\"exampleInputEmail1\" class=\"\">Fullname</label>\n                    <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"\">\n                  </div> -->\n\n            <div class=\"form-group fcl2\">\n              <label for=\"exampleInputPassword1\" class=\"\">Address</label>\n              <input type=\"text\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\">\n            </div>\n\n            <div class=\"form-group\">\n              <div class=\"form-group row fcl3\">\n                <div class=\"form-group col-md-4  \">\n                  <label for=\"exampleInputEmail1\" class=\"\">House Number</label>\n                  <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"\">\n                </div>\n                <div class=\"form-group col-md-8\">\n                  <label for=\"exampleInputPassword1\" class=\"\">Street</label>\n                  <input type=\"text\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\">\n                </div>\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n              \n            </div>\n\n            <div class=\"form-group row fcl3\">\n              <div class=\"form-group col-md-7  \">\n              <label for=\"exampleSelect1\">State</label>\n              <select class=\"form-control\" id=\"exampleSelect1\">\n                <option *ngFor=\"let item of region\">{{ item.default_name }}</option>\n               \n              </select>\n              </div>\n              <div class=\"form-group col-md-5\">\n                <label for=\"exampleInputPassword1\" class=\"\">City</label>\n                <input type=\"text\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\">\n              </div>\n            </div>\n\n            <div class=\"form-group fcl2\">\n              <label for=\"exampleInputPassword1\" class=\"\">Zip/postcode</label>\n              <input type=\"text\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\">\n            </div>\n\n\n\n\n            <div class=\"form-group text-center\">\n              <a href=\"#\" class=\"btn btn-primary blue\">update</a>\n            </div>\n          </form>\n\n        </div>\n        <div class=\"col-sm-2 \">\n        </div>\n      </div>\n      <div class=\"row tab_action\">\n      <div class=\"col-md-4\">\n        <a href=\"#\">\n          <div class=\"card\">\n            <div class=\"card-tab\">\n              <div class=\"order_tab\">\n                \n                <div id=\"action\">\n                  <h4 class=\"card-title\">Address</h4>\n                  <h6 class=\"card-subtitle\">12, Sholakan Street, Sholakan Street</h6><br>\n                  <h6 class=\"card-subtitle\">Lagos</h6>\n                </div>\n              </div>\n            </div>\n          </div>\n        </a>\n      </div>\n\n      <!--  -->\n      <div class=\"col-md-4\">\n        <a href=\"#\">\n          <div class=\"card\">\n            <div class=\"card-tab\">\n              <div class=\"order_tab\">\n\n                <div id=\"action\">\n                  <h4 class=\"card-title\">Address</h4>\n                  <h6 class=\"card-subtitle\">12, Sholakan Street, Sholakan Street</h6><br>\n                  <h6 class=\"card-subtitle\">Lagos</h6>\n                </div>\n              </div>\n            </div>\n          </div>\n        </a>\n      </div>\n\n      <!--  -->\n\n      <div class=\"col-md-4\">\n        <a href=\"#\">\n          <div class=\"card\">\n            <div class=\"card-tab\">\n              <div class=\"order_tab\">\n\n                <div id=\"action\">\n                  <h4 class=\"card-title\">Address</h4>\n                  <h6 class=\"card-subtitle\">12, Sholakan Street, Sholakan Street</h6><br>\n                  <h6 class=\"card-subtitle\">Lagos</h6>\n                </div>\n              </div>\n            </div>\n          </div>\n        </a>\n      </div>\n\n      <!--  -->\n\n      <div class=\"col-md-4\">\n        <a href=\"#\">\n          <div class=\"card\">\n            <div class=\"card-tab\">\n              <div class=\"order_tab\">\n\n                <div id=\"action\">\n                  <h4 class=\"card-title\">Address</h4>\n                  <h6 class=\"card-subtitle\">12, Sholakan Street, Sholakan Street</h6><br>\n                  <h6 class=\"card-subtitle\">Lagos</h6>\n                </div>\n              </div>\n            </div>\n          </div>\n        </a>\n      </div>\n\n\n      <br>\n    </div>\n    <!-- End of content wrapper -->"

/***/ }),

/***/ "./src/app/profile/address-book/address-book.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddressBookComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__address_service__ = __webpack_require__("./src/app/profile/address-book/address_service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { Regions } from './address-service/regions';
var AddressBookComponent = (function () {
    function AddressBookComponent(addressService) {
        var _this = this;
        this.addressService = addressService;
        this.regions = [];
        addressService.getRegions().subscribe(function (response) {
            var data = response.json();
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var region = data_1[_i];
                _this.regions.push(region);
            }
        });
    }
    AddressBookComponent.prototype.ngOnInit = function () {
        jQuery('.new_address').hide();
        jQuery('.address_toggle').click(function () {
            jQuery('.new_address').show('slow');
        });
    };
    AddressBookComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-address-book',
            template: __webpack_require__("./src/app/profile/address-book/address-book.component.html"),
            styles: [__webpack_require__("./src/app/profile/address-book/address-book.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__address_service__["a" /* AddressService */]])
    ], AddressBookComponent);
    return AddressBookComponent;
}());



/***/ }),

/***/ "./src/app/profile/address-book/address-book.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddressBookModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__address_book_component__ = __webpack_require__("./src/app/profile/address-book/address-book.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared__ = __webpack_require__("./src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var addressBookRouting = __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild([
    {
        path: 'account/address',
        component: __WEBPACK_IMPORTED_MODULE_2__address_book_component__["a" /* AddressBookComponent */]
    }
]);
var AddressBookModule = (function () {
    function AddressBookModule() {
    }
    AddressBookModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                addressBookRouting,
                __WEBPACK_IMPORTED_MODULE_3__shared__["a" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__address_book_component__["a" /* AddressBookComponent */]
            ],
            providers: []
        })
    ], AddressBookModule);
    return AddressBookModule;
}());



/***/ }),

/***/ "./src/app/profile/address-book/address-service/address-service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddressService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/_esm5/Rx.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddressService = (function () {
    function AddressService(http) {
        this.http = http;
        this._http = http;
    }
    AddressService.prototype.getRegions = function () {
        return this._http.get('http://magento.amalinze.com/api/getRegions.php');
    };
    AddressService.prototype.getCustomerInfo = function () {
        var email = localStorage.getItem('email');
        return this._http.get('http://magento.amalinze.com/api/getUserDetails.php?email=' + email);
    };
    AddressService.prototype._handleError = function (error) {
        console.error(error.json());
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(error.json() || 'Server error');
    };
    AddressService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], AddressService);
    return AddressService;
}());



/***/ }),

/***/ "./src/app/profile/address-book/address_service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddressService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/_esm5/Rx.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddressService = (function () {
    function AddressService(http) {
        this.http = http;
        this._http = http;
    }
    AddressService.prototype.getRegions = function () {
        return this._http.get('http://magento.amalinze.com/api/getRegions.php');
    };
    AddressService.prototype._handleError = function (error) {
        console.error(error.json());
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(error.json() || 'Server error');
    };
    AddressService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], AddressService);
    return AddressService;
}());



/***/ }),

/***/ "./src/app/profile/profile.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- ============ Your account ============ -->\n<div id=\"content_wrapper\">\n  <br>\n  <div class=\"container-fluid\">\n    <div class=\"row account\">\n      <div class=\"col-md-4 no_padding onset-6\">\n        <div class=\"user_info\">\n          <h3>{{customer_name}}</h3>\n          <p>Lagos, Nigeria</p>\n        </div>\n        <div id=\"user_desc\">\n          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n          </p>\n        </div>\n\n      </div>\n    </div>\n\n\n    <!-- Action Tabs  -->\n    <div class=\"row tab_action\">\n      <div class=\"col-md-4\">\n        <a href=\"/account/track/order\">\n          <div class=\"card\">\n            <div class=\"card-tab\">\n              <div class=\"order_tab\">\n                <div id=\"svg\">\n                  <svg version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n                    x=\"0px\" y=\"0px\" width=\"45px\" viewBox=\"0 0 58 58\" style=\"enable-background:new 0 0 58 58;\" xml:space=\"preserve\">\n                    <g>\n                      <polygon style=\"fill:#A98258;\" points=\"48,0 10,0 0,16 0,58 58,58 58,16  \" />\n                      <polygon style=\"fill:#DAAE86;\" points=\"10,0 0,16 58,16 48,0   \" />\n                      <polygon style=\"fill:#D8B18B;\" points=\"33,54 29,50 25,54 23,52 23,58 35,58 35,52  \" />\n                      <rect x=\"23\" style=\"fill:#F4D5BD;\" width=\"12\" height=\"16\" />\n                      <polygon style=\"fill:#D8B18B;\" points=\"25,21 29,25 33,21 35,23 35,16 23,16 23,23  \" />\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                  </svg>\n                </div>\n                <div id=\"action\">\n                  <h4 class=\"card-title\">Your Orders</h4>\n                  <h6 class=\"card-subtitle\">Track, return or buy something again</h6>\n                </div>\n              </div>\n            </div>\n          </div>\n        </a>\n      </div>\n\n      <!--  -->\n      <div class=\"col-md-4\">\n        <a href=\"/account/customer/edit\">\n          <div class=\"card\">\n            <div class=\"card-tab\">\n              <div class=\"order_tab\">\n                <div id=\"svg\">\n                  <svg version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n                    x=\"0px\" y=\"0px\" width=\"45px\" viewBox=\"0 0 58 58\" style=\"enable-background:new 0 0 58 58;\" xml:space=\"preserve\">\n                    <path style=\"fill:none;stroke:#D8A852;stroke-width:4;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\n  M39,22.866V11.219C39,6.149,34.5,2,29,2s-10,4.149-10,9.219v11.647\" />\n                    <circle style=\"fill:#E7ECED;\" cx=\"29\" cy=\"39\" r=\"19\" />\n                    <path style=\"fill:#C7CAC7;\" d=\"M48,39c0-10.493-8.507-19-19-19v38C39.493,58,48,49.493,48,39z\" />\n                    <path style=\"fill:#424A60;\" d=\"M29,44L29,44c-1.65,0-3-1.35-3-3v-6c0-1.65,1.35-3,3-3h0c1.65,0,3,1.35,3,3v6\n  C32,42.65,30.65,44,29,44z\" />\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                  </svg>\n                </div>\n                <div id=\"action\">\n                  <h4 class=\"card-title\">Login &amp; Security</h4>\n                  <h6 class=\"card-subtitle\">Edit login, name and mobile number</h6>\n                </div>\n              </div>\n            </div>\n          </div>\n        </a>\n      </div>\n\n      <!--  -->\n\n      <div class=\"col-md-4\">\n        <a href=\"/account/address\">\n          <div class=\"card\">\n            <div class=\"card-tab\">\n              <div class=\"order_tab\">\n                <div id=\"svg\">\n                  <svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n                    x=\"0px\" y=\"0px\" width=\"45px\" viewBox=\"0 0 512 512\" style=\"enable-background:new 0 0 512 512;\" xml:space=\"preserve\">\n                    <g>\n                      <path style=\"fill:#424A60;\" d=\"M445.793,512H128V0h317.793c9.754,0,17.655,7.901,17.655,17.655v476.69\n    C463.448,504.099,455.548,512,445.793,512\" />\n                      <path style=\"fill:#586789;\" d=\"M128,512H92.69c-9.754,0-17.655-7.901-17.655-17.655V17.655C75.034,7.901,82.935,0,92.69,0H128V512z\n    \" />\n                      <g>\n                        <path style=\"fill:#CCD0D2;\" d=\"M92.69,52.966h-35.31c-4.873,0-8.828-3.946-8.828-8.828s3.955-8.828,8.828-8.828h35.31\n      c4.873,0,8.828,3.946,8.828,8.828S97.562,52.966,92.69,52.966\" />\n                        <path style=\"fill:#CCD0D2;\" d=\"M92.69,123.586h-35.31c-4.873,0-8.828-3.946-8.828-8.828s3.955-8.828,8.828-8.828h35.31\n      c4.873,0,8.828,3.946,8.828,8.828S97.562,123.586,92.69,123.586\" />\n                        <path style=\"fill:#CCD0D2;\" d=\"M92.69,194.207h-35.31c-4.873,0-8.828-3.946-8.828-8.828c0-4.882,3.955-8.828,8.828-8.828h35.31\n      c4.873,0,8.828,3.946,8.828,8.828C101.517,190.261,97.562,194.207,92.69,194.207\" />\n                        <path style=\"fill:#CCD0D2;\" d=\"M92.69,264.828h-35.31c-4.873,0-8.828-3.946-8.828-8.828s3.955-8.828,8.828-8.828h35.31\n      c4.873,0,8.828,3.946,8.828,8.828S97.562,264.828,92.69,264.828\" />\n                        <path style=\"fill:#CCD0D2;\" d=\"M92.69,335.448h-35.31c-4.873,0-8.828-3.946-8.828-8.828c0-4.882,3.955-8.828,8.828-8.828h35.31\n      c4.873,0,8.828,3.946,8.828,8.828C101.517,331.502,97.562,335.448,92.69,335.448\" />\n                        <path style=\"fill:#CCD0D2;\" d=\"M92.69,406.069h-35.31c-4.873,0-8.828-3.946-8.828-8.828c0-4.882,3.955-8.828,8.828-8.828h35.31\n      c4.873,0,8.828,3.946,8.828,8.828C101.517,402.123,97.562,406.069,92.69,406.069\" />\n                        <path style=\"fill:#CCD0D2;\" d=\"M92.69,476.69h-35.31c-4.873,0-8.828-3.946-8.828-8.828c0-4.882,3.955-8.828,8.828-8.828h35.31\n      c4.873,0,8.828,3.946,8.828,8.828C101.517,472.744,97.562,476.69,92.69,476.69\" />\n                      </g>\n                      <path style=\"fill:#25AE88;\" d=\"M331.034,406.069c0-29.255-23.711-52.966-52.966-52.966s-52.966,23.711-52.966,52.966\n    s23.711,52.966,52.966,52.966S331.034,435.324,331.034,406.069\" />\n                      <path style=\"fill:#CC4B4C;\" d=\"M348.69,344.276v35.31h-24.797c4.511,7.795,7.142,16.825,7.142,26.483\n    c0,7.274-1.474,14.204-4.122,20.515c6.444,3.69,13.815,5.967,21.778,5.967c24.373,0,44.138-19.765,44.138-44.138\n    S373.063,344.276,348.69,344.276\" />\n                      <path style=\"fill:#F0CE49;\" d=\"M260.414,291.31v23.499l19.897,7.239l-11.626,31.938c3.054-0.547,6.179-0.883,9.384-0.883\n    c19.597,0,36.661,10.664,45.824,26.483h24.797V291.31H260.414z\" />\n                      <path style=\"fill:#CC4B4C;\" d=\"M268.687,353.983l11.626-31.929l-66.366-24.152l-24.152,66.357l42.39,15.431\n    C239.838,366.412,253.062,356.781,268.687,353.983\" />\n                      <g>\n                        <path style=\"fill:#586789;\" d=\"M278.069,97.103h-88.276c-4.873,0-8.828-3.946-8.828-8.828s3.955-8.828,8.828-8.828h88.276\n      c4.873,0,8.828,3.946,8.828,8.828S282.942,97.103,278.069,97.103\" />\n                        <path style=\"fill:#586789;\" d=\"M401.655,158.897h-61.793c-4.873,0-8.828-3.946-8.828-8.828s3.955-8.828,8.828-8.828h61.793\n      c4.873,0,8.828,3.946,8.828,8.828S406.528,158.897,401.655,158.897\" />\n                        <path style=\"fill:#586789;\" d=\"M286.901,158.897h-61.793c-4.882,0-8.828-3.946-8.828-8.828s3.946-8.828,8.828-8.828h61.793\n      c4.873,0,8.828,3.946,8.828,8.828S291.774,158.897,286.901,158.897\" />\n                        <path style=\"fill:#586789;\" d=\"M189.793,158.897h-17.655c-4.873,0-8.828-3.946-8.828-8.828s3.955-8.828,8.828-8.828h17.655\n      c4.873,0,8.828,3.946,8.828,8.828S194.666,158.897,189.793,158.897\" />\n                        <path style=\"fill:#586789;\" d=\"M401.655,194.207H384c-4.873,0-8.828-3.946-8.828-8.828c0-4.882,3.955-8.828,8.828-8.828h17.655\n      c4.873,0,8.828,3.946,8.828,8.828C410.483,190.261,406.528,194.207,401.655,194.207\" />\n                        <path style=\"fill:#586789;\" d=\"M348.69,194.207h-88.276c-4.873,0-8.828-3.946-8.828-8.828c0-4.882,3.955-8.828,8.828-8.828h88.276\n      c4.873,0,8.828,3.946,8.828,8.828C357.517,190.261,353.562,194.207,348.69,194.207\" />\n                        <path style=\"fill:#586789;\" d=\"M207.453,194.207h-35.31c-4.882,0-8.828-3.946-8.828-8.828c0-4.882,3.946-8.828,8.828-8.828h35.31\n      c4.873,0,8.828,3.946,8.828,8.828C216.28,190.261,212.326,194.207,207.453,194.207\" />\n                        <path style=\"fill:#586789;\" d=\"M401.655,229.517h-88.276c-4.873,0-8.828-3.946-8.828-8.828s3.955-8.828,8.828-8.828h88.276\n      c4.873,0,8.828,3.946,8.828,8.828S406.528,229.517,401.655,229.517\" />\n                        <path style=\"fill:#586789;\" d=\"M260.414,229.517h-35.31c-4.873,0-8.828-3.946-8.828-8.828s3.955-8.828,8.828-8.828h35.31\n      c4.873,0,8.828,3.946,8.828,8.828S265.287,229.517,260.414,229.517\" />\n                        <path style=\"fill:#586789;\" d=\"M189.793,229.517h-17.655c-4.873,0-8.828-3.946-8.828-8.828s3.955-8.828,8.828-8.828h17.655\n      c4.873,0,8.828,3.946,8.828,8.828S194.666,229.517,189.793,229.517\" />\n                      </g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                  </svg>\n\n                </div>\n                <div id=\"action\">\n                  <h4 class=\"card-title\">Your Addresses</h4>\n                  <h6 class=\"card-subtitle\">Edit your address for your orders and gifts</h6>\n\n                </div>\n              </div>\n            </div>\n          </div>\n        </a>\n      </div>\n\n      <!--  -->\n\n      <!-- <div class=\"col-md-4\">\n        <a href=\"update-card.html\">\n          <div class=\"card\">\n            <div class=\"card-tab\">\n              <div class=\"order_tab\">\n                <div id=\"svg\">\n                  <svg version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n                    x=\"0px\" y=\"0px\" width=\"45px\" viewBox=\"0 0 512 512\" style=\"enable-background:new 0 0 512 512;\" xml:space=\"preserve\">\n                    <g>\n                      <g>\n                        <path style=\"fill:#5D647F;\" d=\"M472,72H40C17.945,72,0,89.945,0,112v288c0,22.055,17.945,40,40,40h432c22.055,0,40-17.945,40-40\n      V112C512,89.945,494.055,72,472,72z\" />\n                      </g>\n                      <g>\n                        <path style=\"fill:#FFD100;\" d=\"M176,232H80c-8.837,0-16-7.163-16-16v-64c0-8.837,7.163-16,16-16h96c8.837,0,16,7.163,16,16v64\n      C192,224.837,184.837,232,176,232z\" />\n                      </g>\n                      <g>\n                        <g>\n                          <path style=\"fill:#B8BAC0;\" d=\"M120,336H80c-8.837,0-16-7.163-16-16v-8c0-8.837,7.163-16,16-16h40c8.837,0,16,7.163,16,16v8\n        C136,328.837,128.837,336,120,336z\" />\n                        </g>\n                        <g>\n                          <path style=\"fill:#B8BAC0;\" d=\"M224,336h-40c-8.837,0-16-7.163-16-16v-8c0-8.837,7.163-16,16-16h40c8.837,0,16,7.163,16,16v8\n        C240,328.837,232.837,336,224,336z\" />\n                        </g>\n                        <g>\n                          <path style=\"fill:#B8BAC0;\" d=\"M328,336h-40c-8.837,0-16-7.163-16-16v-8c0-8.837,7.163-16,16-16h40c8.837,0,16,7.163,16,16v8\n        C344,328.837,336.837,336,328,336z\" />\n                        </g>\n                        <g>\n                          <path style=\"fill:#B8BAC0;\" d=\"M432,336h-40c-8.837,0-16-7.163-16-16v-8c0-8.837,7.163-16,16-16h40c8.837,0,16,7.163,16,16v8\n        C448,328.837,440.837,336,432,336z\" />\n                        </g>\n                      </g>\n                      <g>\n                        <g>\n                          <path style=\"fill:#8A8895;\" d=\"M232,384H72c-4.422,0-8-3.582-8-8s3.578-8,8-8h160c4.422,0,8,3.582,8,8S236.422,384,232,384z\"\n                          />\n                        </g>\n                      </g>\n                      <g>\n                        <g>\n                          <path style=\"fill:#8A8895;\" d=\"M336,384h-72c-4.422,0-8-3.582-8-8s3.578-8,8-8h72c4.422,0,8,3.582,8,8S340.422,384,336,384z\"\n                          />\n                        </g>\n                      </g>\n                      <g>\n                        <path style=\"fill:#FF4F19;\" d=\"M368,216.002C359.211,225.821,346.439,232,332.224,232c-26.51,0-48-21.49-48-48s21.49-48,48-48\n      c14.213,0,26.983,6.177,35.772,15.993\" />\n                      </g>\n                      <g>\n                        <polygon style=\"fill:#FF9500;\" points=\"192,192 112,192 112,176 192,176 192,160 112,160 112,136 96,136 96,232 112,232 112,208 \n      192,208     \" />\n                      </g>\n                      <g>\n                        <circle style=\"fill:#FFD100;\" cx=\"400\" cy=\"184\" r=\"48\" />\n                      </g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                    <g>\n                    </g>\n                  </svg>\n\n                </div>\n                <div id=\"action\">\n                  <h4 class=\"card-title\">Payment Options</h4>\n                  <h6 class=\"card-subtitle\">Edit or add payment options</h6>\n                </div>\n              </div>\n            </div>\n          </div>\n    </a>\n  </div> -->\n\n\n  <br>\n</div>\n<!-- End of content wrapper -->"

/***/ }),

/***/ "./src/app/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__address_book_address_service_address_service__ = __webpack_require__("./src/app/profile/address-book/address-service/address-service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProfileComponent = (function () {
    function ProfileComponent(addressService) {
        var _this = this;
        this.addressService = addressService;
        this.customer_name = "";
        this.region = "";
        this.addressService.getCustomerInfo().subscribe(function (response) {
            var data = response.json();
            console.log(data);
            _this.customer_name = data.firstname + ' ' + data.lastname;
        });
    }
    ProfileComponent.prototype.ngOnInit = function () {
    };
    ProfileComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'profile',
            template: __webpack_require__("./src/app/profile/profile.component.html"),
            styles: [__webpack_require__("./src/app/profile/profile.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__address_book_address_service_address_service__["a" /* AddressService */]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_component__ = __webpack_require__("./src/app/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared__ = __webpack_require__("./src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var profileRouting = __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild([
    {
        path: 'account',
        component: __WEBPACK_IMPORTED_MODULE_2__profile_component__["a" /* ProfileComponent */]
    }
]);
var ProfileModule = (function () {
    function ProfileModule() {
    }
    ProfileModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                profileRouting,
                __WEBPACK_IMPORTED_MODULE_3__shared__["a" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile_component__["a" /* ProfileComponent */]
            ],
            providers: []
        })
    ], ProfileModule);
    return ProfileModule;
}());



/***/ }),

/***/ "./src/app/profile/track-order/track-order.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/profile/track-order/track-order.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- ============ Your account ============ -->\n<div id=\"content_wrapper\">\n  <br>\n  <div class=\"container-fluid\">\n    <div class=\"row account\">\n      <div class=\"col-md-4 no_padding onset-6\">\n        <div class=\"user_info\">\n          <h3>Cassy Doe</h3>\n          <p>Lagos, Nigeria</p>\n        </div>\n        <div id=\"user_desc\">\n          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n          </p>\n        </div>\n\n      </div>\n\n    </div>\n\n\n    <!--   -->\n    <!-- ******************************************************************\n                      Address \n                   *************************************************************-->\n\n\n    <!--  -->\n    <div class=\"row tab_action a_account\">\n      <div class=\"col-sm-2 \">\n        <h5><b>Track Your Order</b></h5>\n      </div>\n      <div class=\"col-sm-8 up_padding\">\n        <!--  -->\n        <form>\n          <br>\n\n          <div class=\"form-group fcl2\">\n            <label for=\"exampleInputPassword1\" class=\"\">Order Number</label>\n            <input type=\"number\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\">\n          </div>\n\n          <div class=\"form-group text-center\">\n            <a href=\"#\" class=\"btn btn-primary blue\">Track</a>\n          </div>\n        </form>\n\n      </div>\n      <div class=\"col-sm-2 \">\n      </div>\n    </div>\n\n  </div>\n\n\n  <br>\n</div>\n<!-- End of content wrapper -->"

/***/ }),

/***/ "./src/app/profile/track-order/track-order.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrackOrderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TrackOrderComponent = (function () {
    function TrackOrderComponent() {
    }
    TrackOrderComponent.prototype.ngOnInit = function () {
    };
    TrackOrderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-track-order',
            template: __webpack_require__("./src/app/profile/track-order/track-order.component.html"),
            styles: [__webpack_require__("./src/app/profile/track-order/track-order.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TrackOrderComponent);
    return TrackOrderComponent;
}());



/***/ }),

/***/ "./src/app/profile/track-order/track-order.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrackOrderModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__track_order_component__ = __webpack_require__("./src/app/profile/track-order/track-order.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared__ = __webpack_require__("./src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var trackOrderRouting = __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild([
    {
        path: 'account/track/order',
        component: __WEBPACK_IMPORTED_MODULE_2__track_order_component__["a" /* TrackOrderComponent */]
    }
]);
var TrackOrderModule = (function () {
    function TrackOrderModule() {
    }
    TrackOrderModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                trackOrderRouting,
                __WEBPACK_IMPORTED_MODULE_3__shared__["a" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__track_order_component__["a" /* TrackOrderComponent */]
            ],
            providers: []
        })
    ], TrackOrderModule);
    return TrackOrderModule;
}());



/***/ }),

/***/ "./src/app/profile/update-contacts/update-contacts.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/profile/update-contacts/update-contacts.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- ============ Your account ============ -->\n<div id=\"content_wrapper\">\n  <br>\n  <div class=\"container-fluid\">\n    <div class=\"row account\">\n    \n      <div class=\"col-md-4 no_padding onset-6\">\n        <div class=\"user_info\">\n          <h3>Cassy Doe</h3>\n          <p>Lagos, Nigeria</p>\n        </div>\n        <div id=\"user_desc\">\n          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n          </p>\n        </div>\n\n      </div>\n\n    </div>\n\n\n    <!--   -->\n    <!-- ******************************************************************\n                      Address \n            *************************************************************-->\n\n\n    <!--  -->\n    <div class=\"row tab_action a_account\">\n      <div class=\"col-sm-2 \">\n        <h5><b>Update Credentials</b></h5>\n      </div>\n      <div class=\"col-sm-8 up_padding\">\n        <!--  -->\n        <form>\n          <br>\n\n          <div class=\"form-group\">\n            <div class=\"form-group row fcl3\">\n              <div class=\"form-group col-md-6  \">\n                <label for=\"exampleInputEmail1\" class=\"\">First Name</label>\n                <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"\">\n              </div>\n              <div class=\"form-group col-md-6\">\n                <label for=\"exampleInputPassword1\" class=\"\">Last Name</label>\n                <input type=\"text\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\">\n              </div>\n            </div>\n          </div>\n\n          <div class=\"form-group fcl2\">\n            <label for=\"exampleInputPassword1\" class=\"\">Email</label>\n            <input type=\"email\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\">\n          </div>\n\n          <div class=\"form-group row fcl3\">\n            <div class=\"form-group col-md-6  \">\n              <label for=\"exampleInputEmail1\" class=\"\">Phone Number</label>\n              <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"\">\n            </div>\n            <div class=\"form-group col-md-6\">\n              <label for=\"exampleInputPassword1\" class=\"\">Password</label>\n              <input type=\"password\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\">\n            </div>\n          </div>\n\n          <div class=\"form-group text-center\">\n            <a href=\"#\" class=\"btn btn-primary blue\">update</a>\n          </div>\n        </form>\n\n      </div>\n      <div class=\"col-sm-2 \">\n      </div>\n    </div>\n\n  </div>\n\n\n  <br>\n</div>\n<!-- End of content wrapper -->"

/***/ }),

/***/ "./src/app/profile/update-contacts/update-contacts.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateContactsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UpdateContactsComponent = (function () {
    function UpdateContactsComponent() {
    }
    UpdateContactsComponent.prototype.ngOnInit = function () {
    };
    UpdateContactsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-update-contacts',
            template: __webpack_require__("./src/app/profile/update-contacts/update-contacts.component.html"),
            styles: [__webpack_require__("./src/app/profile/update-contacts/update-contacts.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], UpdateContactsComponent);
    return UpdateContactsComponent;
}());



/***/ }),

/***/ "./src/app/profile/update-contacts/update-contacts.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateContactModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__update_contacts_component__ = __webpack_require__("./src/app/profile/update-contacts/update-contacts.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared__ = __webpack_require__("./src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var updateContactRouting = __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild([
    {
        path: 'account/customer/edit',
        component: __WEBPACK_IMPORTED_MODULE_2__update_contacts_component__["a" /* UpdateContactsComponent */]
    }
]);
var UpdateContactModule = (function () {
    function UpdateContactModule() {
    }
    UpdateContactModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                updateContactRouting,
                __WEBPACK_IMPORTED_MODULE_3__shared__["a" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__update_contacts_component__["a" /* UpdateContactsComponent */]
            ],
            providers: []
        })
    ], UpdateContactModule);
    return UpdateContactModule;
}());



/***/ }),

/***/ "./src/app/search-results/search-results.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/search-results/search-results.component.html":
/***/ (function(module, exports) {

module.exports = "\n<!-- ============ Search Results ============ -->\n   <div id=\"content_wrapper\">\n       <div class=\"container-fluid\">\n         <div class=\"row\">\n          <!-- search sidebar -->\n            <div class=\"col-md-3 hidden-md-down\" style=\"padding-right: 5px;\">\n              <div class=\"s_sidebar\">\n                <div class=\"filters\">\n                  <h5>Filters</h5> \n\n                </div>\n                <hr>\n\n                <div class=\"category\" >\n                  <h5>Categories</h5>\n                   <ul class=\"list-unstyled components\">\n                    <li class=\"\">\n                        <a href=\"#\" ><i class=\"fa fa-chevron-left\"></i> Clothing</a>\n                     </li>\n                     <li class=\"\">\n                        <a href=\"#\" ><i class=\"fa fa-chevron-left\"></i> Men's Clothing</a>\n                     </li>\n                     <li class=\"\">\n                        <a href=\"#\" ><i class=\"fa fa-chevron-left\"></i> T-shirts</a>\n                     </li>\n                   </ul>\n                </div>\n                <hr>\n\n                <div class=\"price\">\n                  <h5>Price</h5>\n                   <ul class=\"list-unstyled components\">\n                    <li class=\"\">\n                        <a href=\"#\" > Under N5,000</a>\n                     </li>\n                     <li class=\"\">\n                        <a href=\"#\" > Between 5,000 and N10,000</a>\n                     </li>\n                     <li class=\"\">\n                        <a href=\"#\" > Between 10,000 and N30,000</a>\n                     </li>\n                     <li class=\"\">\n                        <a href=\"#\" > Between 30,000 and N60,000</a>\n                     </li>\n                     <li class=\"\">\n                        <a href=\"#\" > N60,000 and above</a>\n                     </li>\n                   </ul>\n                   <div>\n                     <form class=\"form-inline\">\n                      <label class=\"sr-only\" for=\"inlineFormInput\">N-min</label>\n                      <input type=\"text\" class=\"form-control m_form\" id=\"inlineFormInput\" placeholder=\"Min\">\n\n                       <label class=\"sr-only\" for=\"inlineFormInput\">N-max</label>\n                      <input type=\"text\" class=\"form-control m_form\" id=\"inlineFormInput\" placeholder=\"Max\">\n\n                      <button type=\"submit\" class=\"btn btn-primary blue\">Go</button>\n                    </form>\n                   </div>\n                </div>\n                <hr>\n\n                <div class=\"shipping\">\n                  <h5>Shipping</h5> \n                  <div class=\"form-check\">\n                    <label class=\"form-check-label\">\n                      <input type=\"checkbox\" class=\"form-check-input\">\n                      Free\n                    </label>\n                  </div>\n                  \n                  <div class=\"form-check\">\n                      <label class=\"form-check-label\">\n                        <input type=\"checkbox\" class=\"form-check-input\">\n                        International\n                      </label>\n                  </div>\n\n                </div>\n                <hr>\n                \n\n                <div class=\"brands\">\n                  <h5>Brands</h5> \n                  <div class=\"form-check\">\n                    <label class=\"form-check-label\">\n                      <input type=\"checkbox\" class=\"form-check-input\">\n                      Timberland\n                    </label>\n                  </div>\n                  \n                  <div class=\"form-check\">\n                      <label class=\"form-check-label\">\n                        <input type=\"checkbox\" class=\"form-check-input\">\n                        Addidas\n                      </label>\n                  </div>\n\n                  <div class=\"form-check\">\n                    <label class=\"form-check-label\">\n                      <input type=\"checkbox\" class=\"form-check-input\">\n                      Timberland\n                    </label>\n                  </div>\n\n                  <a href=\"#\"><strong>234 more</strong></a>\n                </div>\n                <hr>\n\n               <div class=\"features\">\n                  <h5>Features</h5> \n                  <div class=\"form-check\">\n                    <label class=\"form-check-label\">\n                      <input type=\"checkbox\" class=\"form-check-input\">\n                      Short Sleeves\n                    </label>\n                  </div>\n                  \n                  <div class=\"form-check\">\n                      <label class=\"form-check-label\">\n                        <input type=\"checkbox\" class=\"form-check-input\">\n                        Patterned\n                      </label>\n                  </div>\n\n                </div>\n                <hr>\n\n                <div class=\"discount\">\n                  <h5>Discount</h5> \n                  <div class=\"form-check\">\n                    <label class=\"form-check-label\">\n                      <input type=\"checkbox\" class=\"form-check-input\">\n                      50% or more\n                    </label>\n                  </div>\n                  \n                  <div class=\"form-check\">\n                      <label class=\"form-check-label\">\n                        <input type=\"checkbox\" class=\"form-check-input\">\n                        20% or more\n                      </label>\n                  </div>\n\n                </div>\n                <hr>\n\n                <div class=\"sellers\">\n                  <h5>Sellers</h5> \n                  <div class=\"form-check\">\n                    <label class=\"form-check-label\">\n                      <input type=\"checkbox\" class=\"form-check-input\">\n                      Amazon\n                    </label>\n                  </div>\n                  \n                  <div class=\"form-check\">\n                      <label class=\"form-check-label\">\n                        <input type=\"checkbox\" class=\"form-check-input\">\n                        Multiskills\n                      </label>\n                  </div>\n\n                </div>\n                <hr>\n\n                <div class=\"availability\">\n                  <h5>Availability</h5> \n                  <div class=\"form-check\">\n                    <label class=\"form-check-label\">\n                      <input type=\"checkbox\" class=\"form-check-input\">\n                      Include Out of Stock\n                    </label>\n                  </div>\n                  \n                  <div class=\"form-check\">\n                      <label class=\"form-check-label\">\n                        <input type=\"checkbox\" class=\"form-check-input\">\n                        Limited\n                      </label>\n                  </div>\n\n                </div>\n                <hr>\n\n                <div class=\"colors\">\n                  <h5>Colors</h5> \n                  <div class=\"form-check\">\n                    <label class=\"form-check-label\">\n                      <input type=\"checkbox\" class=\"form-check-input\">\n                      Green\n                    </label>\n                  </div>\n                  \n                  <div class=\"form-check\">\n                      <label class=\"form-check-label\">\n                        <input type=\"checkbox\" class=\"form-check-input\">\n                        Blue\n                      </label>\n                  </div>\n\n                </div>\n                <hr>\n\n                <div class=\"offers\">\n                  <h5>Offers</h5> \n                  <div class=\"form-check\">\n                    <label class=\"form-check-label\">\n                      <input type=\"checkbox\" class=\"form-check-input\">\n                      Bank Offer\n                    </label>\n                  </div>\n                  \n                  <div class=\"form-check\">\n                      <label class=\"form-check-label\">\n                        <input type=\"checkbox\" class=\"form-check-input\">\n                        Special Offer\n                      </label>\n                  </div>\n\n                </div>\n                <hr>\n\n                <div class=\"occasion\">\n                  <h5>Occasion</h5> \n                  <div class=\"form-check\">\n                    <label class=\"form-check-label\">\n                      <input type=\"checkbox\" class=\"form-check-input\">\n                      Beach Wear\n                    </label>\n                  </div>\n                  \n                  <div class=\"form-check\">\n                      <label class=\"form-check-label\">\n                        <input type=\"checkbox\" class=\"form-check-input\">\n                        Sport\n                      </label>\n                  </div>\n\n                </div>\n                \n\n              </div>\n            </div>\n            <!-- Modal for the filter on mobile view -->\n            <div class=\"col hidden-lg-up fil\">\n                \n                <h6 class=\"float-right filter-title\" data-toggle=\"modal\" data-target=\"#modal1\"><img src=\"webfonts/filter.svg\" width=\"20px\"> Filters </h6>\n              \n            </div>\n            <!-- Modal -->\n            <div class=\"modal \" id=\"modal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\" aria-hidden=\"true\">\n              <div class=\"modal-dialog-fil\" role=\"document\">\n                <div class=\"modal-content-fil modal-content-one\">\n                  <div class=\"modal-header\">\n                    <h4 class=\"modal-title\" id=\"exampleModalLongTitle\">Filters</h4>\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                   <span aria-hidden=\"true\">&times;</span>\n            </button>\n                    \n                  </div>\n                  <div class=\"modal-body\">\n                    <div class=\"container\">\n                      <div class=\"row\">\n                        <!--  -->\n                        \n                        <!--  -->\n                        <div class=\"col-md-6\">\n                          <div class=\"price\">\n                          <h5>Price</h5>\n                           <ul class=\"list-unstyled components\">\n                            <li class=\"\">\n                                <a href=\"#\" > Under N5,000</a>\n                             </li>\n                             <li class=\"\">\n                                <a href=\"#\" > Between 5,000 and N10,000</a>\n                             </li>\n                             <li class=\"\">\n                                <a href=\"#\" > Between 10,000 and N30,000</a>\n                             </li>\n                             <li class=\"\">\n                                <a href=\"#\" > Between 30,000 and N60,000</a>\n                             </li>\n                             <li class=\"\">\n                                <a href=\"#\" > N60,000 and above</a>\n                             </li>\n                           </ul>\n                           <div>\n                             <form class=\"form-inline\">\n                              <label class=\"sr-only\" for=\"inlineFormInput\">N-min</label>\n                              <input type=\"text\" class=\"form-control m_form\" id=\"inlineFormInput\" placeholder=\"Min\">\n\n                               <label class=\"sr-only\" for=\"inlineFormInput\">N-max</label>\n                              <input type=\"text\" class=\"form-control m_form\" id=\"inlineFormInput\" placeholder=\"Max\">\n\n                              <button type=\"submit\" class=\"btn btn-primary blue\">Go</button>\n                            </form>\n                           </div>\n                        </div>\n                        <hr>\n                        </div>\n                        <!--  -->\n                        <div class=\"col-md-6\">\n                           <div class=\"shipping\">\n                            <h5>Shipping</h5> \n                            <div class=\"form-check\">\n                              <label class=\"form-check-label\">\n                                <input type=\"checkbox\" class=\"form-check-input\">\n                                Free\n                              </label>\n                            </div>\n                            \n                            <div class=\"form-check\">\n                                <label class=\"form-check-label\">\n                                  <input type=\"checkbox\" class=\"form-check-input\">\n                                  International\n                                </label>\n                            </div>\n\n                          </div>\n                          <hr>\n                        </div>\n                        <!--  -->\n                        <div class=\"col-md-6\">\n                          <div class=\"brands\">\n                            <h5>Brands</h5> \n                            <div class=\"form-check\">\n                              <label class=\"form-check-label\">\n                                <input type=\"checkbox\" class=\"form-check-input\">\n                                Timberland\n                              </label>\n                            </div>\n                            \n                            <div class=\"form-check\">\n                                <label class=\"form-check-label\">\n                                  <input type=\"checkbox\" class=\"form-check-input\">\n                                  Addidas\n                                </label>\n                            </div>\n\n                            <div class=\"form-check\">\n                              <label class=\"form-check-label\">\n                                <input type=\"checkbox\" class=\"form-check-input\">\n                                Timberland\n                              </label>\n                            </div>\n\n                            <a href=\"#\"><strong>234 more</strong></a>\n                          </div>\n                          <hr>\n                        </div>\n                         <!--  -->\n                         <div class=\"col-md-6\">\n                          <div class=\"features\">\n                            <h5>Features</h5> \n                            <div class=\"form-check\">\n                              <label class=\"form-check-label\">\n                                <input type=\"checkbox\" class=\"form-check-input\">\n                                Short Sleeves\n                              </label>\n                            </div>\n                            \n                            <div class=\"form-check\">\n                                <label class=\"form-check-label\">\n                                  <input type=\"checkbox\" class=\"form-check-input\">\n                                  Patterned\n                                </label>\n                            </div>\n\n                          </div>\n                          <hr>\n                        </div>\n                         <!--  -->\n                         <div class=\"col-md-6\">\n                          <div class=\"discount\">\n                          <h5>Discount</h5> \n                            <div class=\"form-check\">\n                              <label class=\"form-check-label\">\n                                <input type=\"checkbox\" class=\"form-check-input\">\n                                50% or more\n                              </label>\n                            </div>\n                            \n                            <div class=\"form-check\">\n                                <label class=\"form-check-label\">\n                                  <input type=\"checkbox\" class=\"form-check-input\">\n                                  20% or more\n                                </label>\n                            </div>\n\n                          </div>\n                          <hr>\n                        </div>\n                         <!--  -->\n                         <div class=\"col-md-6\">\n                          <div class=\"sellers\">\n                          <h5>Sellers</h5> \n                            <div class=\"form-check\">\n                              <label class=\"form-check-label\">\n                                <input type=\"checkbox\" class=\"form-check-input\">\n                                Amazon\n                              </label>\n                            </div>\n                            \n                            <div class=\"form-check\">\n                                <label class=\"form-check-label\">\n                                  <input type=\"checkbox\" class=\"form-check-input\">\n                                  Multiskills\n                                </label>\n                            </div>\n\n                          </div>\n                          <hr>\n                        </div>\n                         <!--  -->\n                         <div class=\"col-md-6\">\n                          <h5>Availability</h5>\n                            <h5>Availability</h5> \n                            <div class=\"form-check\">\n                              <label class=\"form-check-label\">\n                                <input type=\"checkbox\" class=\"form-check-input\">\n                                Include Out of Stock\n                              </label>\n                            </div>\n                            \n                            <div class=\"form-check\">\n                                <label class=\"form-check-label\">\n                                  <input type=\"checkbox\" class=\"form-check-input\">\n                                  Limited\n                                </label>\n                            </div>\n\n                          </div>\n                          <hr>\n                        </div>\n                         <!--  -->\n                       </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              \n          \n\n            <!-- search main -->\n            <div class=\"col-md-12 col-lg-9\" style=\"padding-left: 5px;\">\n              <div class=\" s_main\">\n                <div class=\"col\">\n                  <div class=\"row\">\n                    <div class=\"col bread_crumb\">\n                      <ol class=\"breadcrumb\">\n                        <li class=\"breadcrumb-item\"><a href=\"#\">Home</a></li>\n                        <li class=\"breadcrumb-item\"><a href=\"#\">Clothing</a></li>\n                        <li class=\"breadcrumb-item\">Men's Clothing</li>\n                        <li class=\"breadcrumb-item active\">T-shirts</li>\n                      </ol>\n                    </div>\n                  </div>\n\n                  <div class=\"row\">\n                    <div class=\"col-sm-8\">\n                      <div id=\"search_result\">\n                        <h4>Showing 1 - 24 of 4,077 results for \"Men's T-shirt\" </h4>\n                      </div>\n                    </div>\n                    <div class=\"col-sm-4\">\n                      <div id=\"sort_by\" class=\"float-right\">\n                        <div class=\"dropdown\">\n                          Sort by <a class=\"btn btn-secondary dropdown-toggle\" href=\"https://example.com\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                            Featured\n                          </a>\n\n                          <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">\n                            <a class=\"dropdown-item\" href=\"#\">Price</a>\n                            <a class=\"dropdown-item\" href=\"#\">Size</a>\n                            <a class=\"dropdown-item\" href=\"#\">Brand</a>\n                          </div>\n                          </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n                 <hr>\n\n\n                <div class=\"col\">\n                  <div class=\"row\"> <!-- first row-->\n                    <div class=\"col-md-3\">\n                      \n                        <!-- Card  -->\n                        <div class=\"card\">\n                          <a href=\"#\">\n                          <img class=\"card-img-top\" src=\"assets/images/demo1.png\" alt=\"Card image cap\"></a>\n                          <div class=\"card-block\">\n                            <h6 class=\"card-title text-left\">Reebok Solid Men's Round Neck Blue T-shirt</h6>\n                            <section id=\"rating\" class=\"text-left\">\n                              \n                              <p><i class=\"fa fa-star-half-full\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i></p>\n\n                              <p id=\"reviews\">4,300</p>\n                            </section>\n                            <h6 id=\"price\" class=\"text-left\">N84,000</h6>\n                         </div>    \n                       </div>\n\n                    </div>\n\n                    <div class=\"col-md-3\">\n                      \n                        <!-- Card  -->\n                        <div class=\"card\">\n                          <a href=\"#\">\n                          <img class=\"card-img-top\" src=\"assets/images/demo1.png\" alt=\"Card image cap\"></a>\n                          <div class=\"card-block\">\n                            <h6 class=\"card-title text-left\">Reebok Solid Men's Round Neck Blue T-shirt</h6>\n                            <section id=\"rating\" class=\"text-left\">\n                              \n                              <p><i class=\"fa fa-star-half-full\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i></p>\n\n                              <p id=\"reviews\">4,300</p>\n                            </section>\n                            <h6 id=\"price\" class=\"text-left\">N84,000</h6>\n                         </div>    \n                       </div>\n\n                    </div>\n\n                    <div class=\"col-md-3\">\n                      \n                        <!-- Card  -->\n                        <div class=\"card\">\n                          <a href=\"#\">\n                          <img class=\"card-img-top\" src=\"assets/images/demo1.png\" alt=\"Card image cap\"></a>\n                          <div class=\"card-block\">\n                            <h6 class=\"card-title text-left\">Reebok Solid Men's Round Neck Blue T-shirt</h6>\n                            <section id=\"rating\" class=\"text-left\">\n                              \n                              <p><i class=\"fa fa-star-half-full\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i></p>\n\n                              <p id=\"reviews\">4,300</p>\n                            </section>\n                            <h6 id=\"price\" class=\"text-left\">N84,000</h6>\n                         </div>    \n                       </div>\n\n                    </div>\n\n\n                    <div class=\"col-md-3\">\n                      \n                        <!-- Card  -->\n                        <div class=\"card\">\n                          <a href=\"#\">\n                          <img class=\"card-img-top\" src=\"assets/images/demo1.png\" alt=\"Card image cap\"></a>\n                          <div class=\"card-block\">\n                            <h6 class=\"card-title text-left\">Reebok Solid Men's Round Neck Blue T-shirt</h6>\n                            <section id=\"rating\" class=\"text-left\">\n                              \n                              <p><i class=\"fa fa-star-half-full\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i></p>\n\n                              <p id=\"reviews\">4,300</p>\n                            </section>\n                            <h6 id=\"price\" class=\"text-left\">N84,000</h6>\n                         </div>    \n                       </div>\n\n                    </div>\n\n\n                  </div>\n\n                  <!--Second row-->\n                  <div class=\"row\">\n                    <div class=\"col-md-3\">\n                      \n                        <!-- Card  -->\n                        <div class=\"card\">\n                          <a href=\"#\">\n                          <img class=\"card-img-top\" src=\"assets/images/demo1.png\" alt=\"Card image cap\"></a>\n                          <div class=\"card-block\">\n                            <h6 class=\"card-title text-left\">Reebok Solid Men's Round Neck Blue T-shirt</h6>\n                            <section id=\"rating\" class=\"text-left\">\n                              \n                              <p><i class=\"fa fa-star-half-full\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i></p>\n\n                              <p id=\"reviews\">4,300</p>\n                            </section>\n                            <h6 id=\"price\" class=\"text-left\">N84,000</h6>\n                         </div>    \n                       </div>\n\n                    </div>\n\n                    <div class=\"col-md-3\">\n                      \n                        <!-- Card  -->\n                        <div class=\"card\">\n                          <a href=\"#\">\n                          <img class=\"card-img-top\" src=\"assets/images/demo1.png\" alt=\"Card image cap\"></a>\n                          <div class=\"card-block\">\n                            <h6 class=\"card-title text-left\">Reebok Solid Men's Round Neck Blue T-shirt</h6>\n                            <section id=\"rating\" class=\"text-left\">\n                              \n                              <p><i class=\"fa fa-star-half-full\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i></p>\n\n                              <p id=\"reviews\">4,300</p>\n                            </section>\n                            <h6 id=\"price\" class=\"text-left\">N84,000</h6>\n                         </div>    \n                       </div>\n\n                    </div>\n\n                    <div class=\"col-md-3\">\n                      \n                        <!-- Card  -->\n                        <div class=\"card\">\n                          <a href=\"#\">\n                          <img class=\"card-img-top\" src=\"assets/images/demo1.png\" alt=\"Card image cap\"></a>\n                          <div class=\"card-block\">\n                            <h6 class=\"card-title text-left\">Reebok Solid Men's Round Neck Blue T-shirt</h6>\n                            <section id=\"rating\" class=\"text-left\">\n                              \n                              <p><i class=\"fa fa-star-half-full\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i></p>\n\n                              <p id=\"reviews\">4,300</p>\n                            </section>\n                            <h6 id=\"price\" class=\"text-left\">N84,000</h6>\n                         </div>    \n                       </div>\n\n                    </div>\n\n\n                    <div class=\"col-md-3\">\n                      \n                        <!-- Card  -->\n                        <div class=\"card\">\n                          <a href=\"#\">\n                          <img class=\"card-img-top\" src=\"assets/images/demo1.png\" alt=\"Card image cap\"></a>\n                          <div class=\"card-block\">\n                            <h6 class=\"card-title text-left\">Reebok Solid Men's Round Neck Blue T-shirt</h6>\n                            <section id=\"rating\" class=\"text-left\">\n                              \n                              <p><i class=\"fa fa-star-half-full\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i></p>\n\n                              <p id=\"reviews\">4,300</p>\n                            </section>\n                            <h6 id=\"price\" class=\"text-left\">N84,000</h6>\n                         </div>    \n                       </div>\n\n                    </div>\n\n\n                  </div>\n                </div> \n              </div>\n            </div>\n         </div>\n       </div>\n       <br>\n   </div><!-- End of content wrapper -->\n\n        <script src=\"assets/js/ie10-viewport-bug-workaround.js\"></script>\n\n\n"

/***/ }),

/***/ "./src/app/search-results/search-results.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchResultsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SearchResultsComponent = (function () {
    function SearchResultsComponent() {
    }
    SearchResultsComponent.prototype.ngOnInit = function () {
    };
    SearchResultsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-search-results',
            template: __webpack_require__("./src/app/search-results/search-results.component.html"),
            styles: [__webpack_require__("./src/app/search-results/search-results.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SearchResultsComponent);
    return SearchResultsComponent;
}());



/***/ }),

/***/ "./src/app/search-results/search-results.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchResultsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search_results_component__ = __webpack_require__("./src/app/search-results/search-results.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared__ = __webpack_require__("./src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var searchResultsRouting = __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild([
    {
        path: 'search-results',
        component: __WEBPACK_IMPORTED_MODULE_2__search_results_component__["a" /* SearchResultsComponent */]
    }
]);
var SearchResultsModule = (function () {
    function SearchResultsModule() {
    }
    SearchResultsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                searchResultsRouting,
                __WEBPACK_IMPORTED_MODULE_3__shared__["a" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__search_results_component__["a" /* SearchResultsComponent */]
            ],
            providers: []
        })
    ], SearchResultsModule);
    return SearchResultsModule;
}());



/***/ }),

/***/ "./src/app/services/Serv.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/_esm5/Rx.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CategoryService = (function () {
    function CategoryService(http) {
        this.http = http;
        this._http = http;
    }
    CategoryService.prototype.getMenuCategories = function () {
        return this._http.get('http://magento.amalinze.com/api/getMenuCategories.php');
    };
    CategoryService.prototype.getAnyCategoryProducts = function (id) {
        return this._http.get('http://magento.amalinze.com/api/getCategoryProducts.php?id=' + id);
    };
    CategoryService.prototype.getGeneratorService = function () {
        return this._http.get('http://magento.amalinze.com/api/getCategoryProducts.php?id=7');
    };
    CategoryService.prototype.getCleaningService = function () {
        return this._http.get('http://magento.amalinze.com/api/getCategoryProducts.php?id=8');
    };
    CategoryService.prototype.getTravelService = function () {
        return this._http.get('http://magento.amalinze.com/api/getCategoryProducts.php?id=15');
    };
    CategoryService.prototype.getEventService = function () {
        return this._http.get('http://magento.amalinze.com/api/getCategoryProducts.php?id=14');
    };
    CategoryService.prototype.getGraphicService = function () {
        return this._http.get('http://magento.amalinze.com/api/getCategoryProducts.php?id=18');
    };
    CategoryService.prototype.getPlumbingService = function () {
        return this._http.get('http://magento.amalinze.com/api/getCategoryProducts.php?id=17');
    };
    CategoryService.prototype._handleError = function (error) {
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(error.json() || 'Server error');
    };
    CategoryService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], CategoryService);
    return CategoryService;
}());



/***/ }),

/***/ "./src/app/services/services.component.css":
/***/ (function(module, exports) {

module.exports = "/*---------------------------------------------\r\n Services-Homepage\r\n------------------------------------------------*/\r\n.recom_services h4{\r\n  margin: 20px 20px 20px 0px;\r\n  font-weight: 500;\r\n}\r\n.card {\r\n\tmargin-top: 20px;\r\n\t-webkit-box-sizing: border-box;\r\n\t        box-sizing: border-box;\r\n\t-webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);\r\n\t        box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);\r\n}\r\n.card img {\r\n\tborder-radius: 5px;\r\n}\r\n.recom_services img {\r\n  height: 190px;\r\n  border-radius: 5px;\r\n}\r\n.recom_services p{\r\n  margin: 10px;\r\n  color: #343434; \r\n}\r\n.resCarousel-inner .item .tile {\r\n    background: white;\r\n    -webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);\r\n            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);\r\n    margin: 5px;\r\n    padding: 10px;\r\n    border-radius: 5px;\r\n}\r\n@media(max-width: 767px){\r\n\t.p8 {\r\n\t\tdisplay: none;\r\n\t}\r\n}"

/***/ }),

/***/ "./src/app/services/services.component.html":
/***/ (function(module, exports) {

module.exports = " <!-- Services recommended -->\n   <div class=\"container-fluid recom_services\">\n    <div class=\"row\">\n         <h4 >Recomended Services for you</h4>\n    </div>\n    <div class=\"row\"> \n       <!-- first row-->\n        <div class=\"col-sm-6 hidden-md-up\">\n          \n            <!-- Card  -->\n            <a href=\"#\"><div class=\"card\">\n              \n              <img class=\"card-img-top img-fluid\" src=\"assets/images/services/visa.jpg\" alt=\"Card image cap \">\n              <div class=\"card-block\">\n                <h6 class=\"card-title text-center\">Visa</h6>\n             </div>    \n           </div></a>\n        </div>\n\n            <!-- // -->\n          <div class=\"hidden-sm-down p8\">   \n            <div class=\"resCarousel\" data-items=\"1-3-4-5\" data-slide=\"5\" data-speed=\"900\" data-interval=\"none\" data-load=\"3\" data-animator=\"lazy\">\n                    <div class=\"resCarousel-inner\" id=\"eventLoad\">\n\n                        <div class=\"item\" *ngFor=\"let item of recommended_services\">\n                            <div class=\"tile\">\n                                <div>\n                                    <a href=\"#\"><img src=\"{{ item.image_path }}\" class=\"img-fluid service_img\">\n                                    <p class=\"text-center\">{{ item.name }}</p></a>\n                                </div>\n                               \n                            </div>\n                        </div>\n\n                        \n\n                    </div>\n                    <button class='btn btn-default leftRs'><i class=\"fa fa-chevron-left\"></i></button>\n                    <button class='btn btn-default rightRs'><i class=\"fa fa-chevron-right\"></i></button>\n                \n               </div> \n              </div>     \n            </div>  \n          </div>\n\n   <!-- Services All-->"

/***/ }),

/***/ "./src/app/services/services.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Serv_service__ = __webpack_require__("./src/app/services/Serv.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ServicesComponent = (function () {
    function ServicesComponent(servServices) {
        var _this = this;
        this.servServices = servServices;
        this.recommended_services = [];
        this.repairs_maintenance = [];
        // Service Category
        servServices.getPlumbingService().subscribe(function (response) {
            var data = response.json();
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var plumbing = data_1[_i];
                _this.recommended_services.push(plumbing);
            }
        });
        //
        servServices.getCleaningService().subscribe(function (response) {
            var data = response.json();
            for (var _i = 0, data_2 = data; _i < data_2.length; _i++) {
                var cleaning = data_2[_i];
                _this.recommended_services.push(cleaning);
            }
        });
        //
        servServices.getGeneratorService().subscribe(function (response) {
            var data = response.json();
            for (var _i = 0, data_3 = data; _i < data_3.length; _i++) {
                var generator = data_3[_i];
                _this.recommended_services.push(generator);
            }
        });
        //
        servServices.getTravelService().subscribe(function (response) {
            var data = response.json();
            for (var _i = 0, data_4 = data; _i < data_4.length; _i++) {
                var travel = data_4[_i];
                _this.recommended_services.push(travel);
            }
        });
        //
        servServices.getGraphicService().subscribe(function (response) {
            var data = response.json();
            for (var _i = 0, data_5 = data; _i < data_5.length; _i++) {
                var graphic = data_5[_i];
                _this.recommended_services.push(graphic);
            }
        });
        //
        servServices.getEventService().subscribe(function (response) {
            var data = response.json();
            for (var _i = 0, data_6 = data; _i < data_6.length; _i++) {
                var event_1 = data_6[_i];
                _this.recommended_services.push(event_1);
            }
        });
    }
    ServicesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-services',
            template: __webpack_require__("./src/app/services/services.component.html"),
            styles: [__webpack_require__("./src/app/services/services.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__Serv_service__["a" /* CategoryService */]])
    ], ServicesComponent);
    return ServicesComponent;
}());



/***/ }),

/***/ "./src/app/shared/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__layout__ = __webpack_require__("./src/app/shared/layout/index.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_module__ = __webpack_require__("./src/app/shared/shared.module.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__shared_module__["a"]; });




/***/ }),

/***/ "./src/app/shared/layout/footer/footer.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/shared/layout/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = " <footer class=\"panel-footer container-fluid\">\n          <div class=\"row\">\n            <section id=\"navigation\" class=\"col-md-3\">\n              <h4>Navigate</h4>\n              <br>\n              <ul class=\"nav-item\">\n                <li class=\"nav-link\">\n                  <a href=\"\"><h6>About Us</h6></a>\n                </li>\n                <li class=\"nav-link\">\n                  <a href=\"\"><h6>Contact Us</h6></a>\n                </li>\n                <li class=\"nav-link\">\n                  <a href=\"\"><h6>Shipping and Returns</h6></a>\n                </li>\n              </ul>\n            </section>\n            <section id=\"category\" class=\"col-md-3\">\n               <h4>Category</h4>\n               <br>\n               <ul class=\"nav-item\">\n                <li class=\"nav-link\">\n                  <a href=\"\"><h6>Women</h6></a>\n                </li>\n                <li class=\"nav-link\">\n                  <a href=\"\"><h6>Men</h6></a>\n                </li>\n                <li class=\"nav-link\">\n                  <a href=\"\"><h6>Electronics</h6></a>\n                </li>\n              </ul>\n            </section>\n            <section id=\"popular_brands\" class=\"col-md-3\">\n               <h4>Popular Brands</h4>\n               <br>\n               <ul class=\"nav-item\">\n                <li class=\"nav-link\">\n                  <a href=\"\"><h6>Dolce &amp; Gabbana</h6></a>\n                </li>\n                <li class=\"nav-link\">\n                  <a href=\"\"><h6>Gucci</h6></a>\n                </li>\n                <li class=\"nav-link\">\n                  <a href=\"\"><h6>Versace</h6></a>\n                </li>\n                <li class=\"nav-link\">\n                  <a href=\"\"><h6>Channel</h6></a>\n                </li>\n                <li class=\"nav-link\">\n                  <a href=\"\"><h6>View All</h6></a>\n                </li>\n              </ul>\n            </section>\n            <section id=\"address\" class=\"col-md-3\">\n               <h4>13A Bishop Dimieri Street <br> GRA Phase 2, PortHarcourt <br> Nigeria</h4>\n               <br>\n               <ul class=\"nav-item\">\n                <li class=\"nav-link\">\n                  <a href=\"\"><h6>info@multiskills-ng.com</h6></a>\n                </li>\n                <li class=\"nav-link\">\n                  <a href=\"\"><h6>+2347035785558</h6></a>\n                </li>\n              </ul>\n            </section> \n           </div>\n            <hr>\n        </footer>\n        <div class=\"copyright container-fluid\">\n            <div class=\"row\">\n              <div class=\"col-sm-6\">\n                <section id=\"copy\"><p>Copyright 2017 Amalinze Limited. All Rights Reserved</p></section>\n              </div>\n              <div class=\"col-sm-6\">\n                <section id=\"social\">\n                 <i class=\"fab fa-facebook-square \"></i>\n                 <i class=\"fab fa-twitter \"></i>\n               </section>\n              </div>   \n            </div>\n         </div>\n"

/***/ }),

/***/ "./src/app/shared/layout/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-footer',
            template: __webpack_require__("./src/app/shared/layout/footer/footer.component.html"),
            styles: [__webpack_require__("./src/app/shared/layout/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/shared/layout/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__sidebar_sidebar_component__ = __webpack_require__("./src/app/shared/layout/sidebar/sidebar.component.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__main_nav_main_nav_component__ = __webpack_require__("./src/app/shared/layout/main-nav/main-nav.component.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login_component__ = __webpack_require__("./src/app/shared/layout/login/login.component.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__signup_signup_component__ = __webpack_require__("./src/app/shared/layout/signup/signup.component.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__footer_footer_component__ = __webpack_require__("./src/app/shared/layout/footer/footer.component.ts");
/* unused harmony namespace reexport */







/***/ }),

/***/ "./src/app/shared/layout/login/login.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/shared/layout/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "  <!-- Login Modal -->\n    <div class=\"modal fade\" id=\"amalinzeModalLogin\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\" aria-hidden=\"true\">\n      <div class=\"modal-dialog \" role=\"document\">\n        <div class=\"modal-content ml-con\">\n          \n          <div class=\"modal-body md-rem\">\n       <!-- Login -->\n            <div class=\"container\" id=\"login_anchor\">\n              <div class=\"row\">\n                <div class=\"col-5 ml-col\" >\n                  <div id=\"side_img\">\n                    <img src=\"assets/images/side.png\" class=\"img-fluid\">\n                  </div>\n                  <section id=\"sign_up_text\">\n                    <h4>Login</h4>\n                    \n                    <p>Get your acess to your Orders, Wishlist and Recommendations.</p>\n                  </section>\n                  \n                </div>\n                <div class=\"col-7 \" id=\"sign_up\">  \n                   <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                      <img src=\"assets/webfonts/cross-out.svg\" width=\"20px\">\n                    </button>\n                    <br><br>\n                    <div  *ngIf=\"canLoad\"><img src=\"assets/images/loader.gif\"/></div>\n\n                  <form id=\"sign_in_form\">\n                    <div class=\"form-group\">\n                      <label for=\"loginEmail1\" id=\"form_field\">Email address</label>\n                      <input type=\"email\" class=\"form-control\" id=\"loginEmail1\" aria-describedby=\"emailHelp\" placeholder=\"\">\n                      \n                    </div>\n                    <div class=\"form-group\">\n                      <label for=\"loginPassword\" id=\"form_field\">Password</label>\n                      <input type=\"Password\" class=\"form-control\" id=\"loginPassword\" placeholder=\"\">\n                    </div>\n                  </form>\n                  <div id=\"sign_up_btn\">\n                    <button class=\"btn btn-primary blue_sign\">Log in</button>\n                    <p>Don't have an account? <a  id=\"show_signup\">Sign Up</a></p>\n                  </div>\n                  \n                  \n                </div>\n              </div>\n            </div>\n\n \n                  \n                </div>\n              </div>\n            </div>\n             </div>\n\n\n\n            "

/***/ }),

/***/ "./src/app/shared/layout/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoginComponent = (function () {
    function LoginComponent() {
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-login',
            template: __webpack_require__("./src/app/shared/layout/login/login.component.html"),
            styles: [__webpack_require__("./src/app/shared/layout/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/shared/layout/main-nav/main-nav.component.css":
/***/ (function(module, exports) {

module.exports = "\r\n.drop_down {\r\n    /* position:absolute;\r\n    top: 40px;\r\n    right: 0px; */\r\n    background: #fff;\r\n    border: 1px solid #c0c0c0;\r\n    z-index: 999;\r\n    width: 92%;\r\n    height:600px;\r\n    vertical-align: middle;\r\n    white-space: nowrap;\r\n    position: absolute;\r\n    top: 69px;\r\n    right: 22px;\r\n}\r\n\r\n.drop_down p {\r\n     z-index: 1;\r\n     position: relative;\r\n}"

/***/ }),

/***/ "./src/app/shared/layout/main-nav/main-nav.component.html":
/***/ (function(module, exports) {

module.exports = "               <nav class=\"main_nav\"> \n                  \n                  <!-- Mobile View Nav -->\n                  <div class=\"container-fluid hidden-lg-up\">\n                      <div class=\"row\">\n                        <div class=\"col\">\n                          <div class=\"hamburger_menu\" id=\"sidebarCollapse\">&#9776;</div>\n                          <a href=\"#\" class=\"navbar-brand\"><div id=\"logo_img\"></div></a>    \n                        </div>\n                        <div class=\"col mobile_view\">\n                          <div class=\"float-right\">\n                            <ul class=\"nav\">\n                               <li class=\"nav-item p-2 account_menu dropdown\">\n                                  <a class=\"nav-link dropdown-toggle btnn dropbtn\"  id=\"dropdown02\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><img src=\"assets/webfonts/user.svg\" width=\"20px\"></a>\n                                      <div class=\"dropdown-content1 text-center\">       \n                                             <a class=\"dropdown-item\" href=\"#\" data-toggle=\"modal\" data-target=\"#exampleModalSignUp\">Sign Up</a>\n                                        <a class=\"dropdown-item\" href=\"#\" data-toggle=\"modal\" data-target=\"#exampleModalLogin\">Login</a>\n                                      </div>\n                               </li>\n                               <li class=\"nav-item basket_menu\" >\n                                <a href=\"\"><img src=\"assets/webfonts/shopping-cart.svg\" width=\"20px\"> </a>\n                                <span class=\"badge badge-pill badge-danger\">9+</span>\n                               </li>\n                            </ul>\n                          </div>\n                        </div>          \n                      </div>\n                     \n                    </div> \n                     <form class=\"form-group osh_search hidden-lg-up\">\n                        <input class=\"form-control mr-sm-2\" type=\"text\" placeholder=\"Search here...\">\n                      </form>\n\n                    <!-- End of Mobile View Nav -->\n\n                    <!-- Desktop view nav -->\n                    <div class=\"container-fluid hidden-md-down\">\n                      <div class=\"row\">\n                        <div class=\"col-md-4 col-sm-4\">     \n                          <a href=\"\" class=\"navbar-brand\"><div id=\"logo_img\"></div></a>\n                        </div>\n                        <div class=\"col-md-8 col-sm-8 \">\n                         <div class=\"row\">\n                          <div class=\"col-sm-7\">\n                            <ul class=\"nav  nav-pill nav-fill\">\n                               <li class=\"nav-item p-2 search_menu\">\n                                  <div class=\"box\">\n                                      <div class=\"container-2\">\n                                        <form action=\"\" method=\"\">\n                                          <a href=\"\"><span class=\"icon\"><i class=\"fa fa-search\"></i></span></a>\n                                          <input #search type=\"search\" (keyup)=\"onKey(search.value)\" id=\"search\" placeholder=\"Search...\" />\n                                          \n                                        </form>\n                                      </div>\n                                      <div class=\"drop_down\" *ngIf=\"showSearchResultsDiv\">\n                                          <ul>\n                                          \n                                          <li *ngFor=\"let sr of searchResults\"><img src=\"{{sr.image_path}}\" width=\"50\" height=\"50\"/><a href=\"/product-detail/{{sr.id}}\" >{{sr.name}}</a></li>\n                                         </ul>\n                                      </div>\n                                    </div>\n                               </li>\n                            </ul>\n                            </div>\n                            <div class=\"col-md-5\">\n                                <ul class=\"nav nav-pill nav-fill\">\n                                   <li class=\"nav-item p-2 account_menu dropdown\"> \n                                    <a class=\"nav-link dropdown-toggle btnn dropbtn\"  id=\"dropdown02\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><img src=\"assets/webfonts/user.svg\" width=\"20px\" ><span *ngIf=\"!(isLoggedIn | async)\"> Your Account</span>\n                                      <span *ngIf=\"(isLoggedIn | async)\">Hello, {{ customerFirstName}}</span>\n                                    </a>\n\n                                    <div class=\"dropdown-content1\">       \n                                             <a class=\"dropdown-item\" href=\"/signup\" *ngIf=\"!(isLoggedIn | async)\"  >Sign Up</a>\n                                        <a class=\"dropdown-item\" href=\"/login\" *ngIf=\"!(isLoggedIn | async)\" >Login</a>\n                                        <a class=\"dropdown-item\" href=\"/account\" *ngIf=\"(isLoggedIn | async)\" >My Account</a>\n                                        <a class=\"dropdown-item\" href=\"#\" (click)=\"logOut()\"\n                                         *ngIf=\"(isLoggedIn | async)\" >Logout</a>\n\n\n                                      </div>\n                                  </li>\n                                  <li class=\"nav-item basket_menu\" >\n                                    <div id=\"basket_icon\">\n                                      <a href=\"\"><img src=\"assets/webfonts/shopping-cart.svg\" width=\"20px\" > Cart </a>\n                                      <span class=\"badge badge-pill badge-danger\">9+</span>\n                                    </div>\n                                  </li>\n                                </ul>\n                           </div>\n                         </div>\n                       </div>   \n                      </div>\n                      <hr>\n                    </div>\n                  </nav> \n                                \n                  <!-- Sub Nav -displays on Medium View and above -->\n                  <nav class=\"navbar-toggleable-md\"> \n                  <div class=\"collapse navbar-collapse menu\" id=\"navbarsExampleDefault\"> \n                    <ul class=\"navbar-nav nav-fill mr-auto sub_menu\">\n                          <li class=\"nav-item dropdown \">\n                        <a class=\"nav-link active dropdown-toggle btnn dropbtn\"  id=\"dropdown01\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Services</a>\n                        <div class=\"dropdown-content\">   \n                            <div class=\"row\">\n                              <div class=\"column\">\n                                <h6>Recommedned Services</h6>\n                                <a href=\"/category\" *ngFor=\"let serviceCategory of  serviceCategories\">{{serviceCategory.name}}</a>\n                                <h6>Repairs & Maintenance</h6>\n                               <a href=\"{{recommendedService.link}\" *ngFor=\"let recommendedService of  recommendedServices\">{{recommendedService.name}}</a>\n\n                              </div>\n                     \n                            </div>\n                          </div>     \n                      </li>\n                      <li class=\"nav-item \" *ngFor=\"let topCat of  topCategories\">\n                        <a class=\"nav-link\" href=\"/category/{{topCat.id}}\">{{topCat.name}} </a>\n                      </li>\n                     \n                    </ul>\n                    \n                  </div>\n                </nav>\n<!-- ====================  End of Navigation -->"

/***/ }),

/***/ "./src/app/shared/layout/main-nav/main-nav.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainNavComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_Serv_service__ = __webpack_require__("./src/app/services/Serv.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__loginpage_auth_service__ = __webpack_require__("./src/app/loginpage/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__searchsuggestions_service__ = __webpack_require__("./src/app/shared/layout/main-nav/searchsuggestions.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MainNavComponent = (function () {
    function MainNavComponent(authService, searchService, router, bannersMenuService) {
        this.authService = authService;
        this.searchService = searchService;
        this.router = router;
        this.bannersMenuService = bannersMenuService;
        this.topCategories = [];
        this.serviceCategories = [];
        this.recommendedServices = [];
        this.searchResults = [];
        this.showSearchResultsDiv = false;
        this.isLoggedIn = authService.isLoggedIn();
        if (authService.ifLoggedIn)
            this.customerFirstName = localStorage.getItem('firstname');
    }
    MainNavComponent.prototype.logOut = function () {
        this.authService.logout();
    };
    MainNavComponent.prototype.onKey = function (value) {
        var _this = this;
        if (value != '') {
            this.searchService.getResults(value).subscribe(function (response) {
                var data = response.json();
                _this.searchResults = [];
                var _loop_1 = function (product) {
                    product.custom_attributes.forEach(function (attr) {
                        if (attr.attribute_code === 'small_image') {
                            product.image_path = 'http://magento.amalinze.com/pub/media/catalog/product' + attr.value;
                        }
                    });
                    _this.searchResults.push(product);
                };
                for (var _i = 0, _a = data.items; _i < _a.length; _i++) {
                    var product = _a[_i];
                    _loop_1(product);
                }
            });
            this.showSearchResultsDiv = true;
        }
        else {
            this.showSearchResultsDiv = false;
            this.searchResults = [];
        }
    };
    MainNavComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.authService.ifLoggedIn) {
            this.router.navigateByUrl("/login");
        }
        this.bannersMenuService.getMenuCategories().subscribe(function (response) {
            var data = response.json();
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var topCat = data_1[_i];
                _this.topCategories.push(topCat);
            }
        });
        this.serviceCategories = [
            { name: 'Event', link: '/category' },
            { name: 'Make Up', link: '/category' },
            { name: 'T-shirts', link: '/category' },
            { name: 'Graphics Design', link: '/category' },
            { name: 'Visa', link: '/category' },
        ];
        this.recommendedServices = [
            { name: 'Fumigation', link: '/category' },
            { name: 'Laptop Repair', link: '/category' },
            { name: 'Cleaning', link: '/category' },
            { name: 'Generator', link: '/category' },
        ];
    };
    MainNavComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-main-nav',
            template: __webpack_require__("./src/app/shared/layout/main-nav/main-nav.component.html"),
            styles: [__webpack_require__("./src/app/shared/layout/main-nav/main-nav.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__loginpage_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_3__searchsuggestions_service__["a" /* SearchSuggestionsService */], __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_1__services_Serv_service__["a" /* CategoryService */]])
    ], MainNavComponent);
    return MainNavComponent;
}());



/***/ }),

/***/ "./src/app/shared/layout/main-nav/searchsuggestions.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchSuggestionsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/_esm5/Rx.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SearchSuggestionsService = (function () {
    function SearchSuggestionsService(http) {
        this.http = http;
        this._http = http;
    }
    SearchSuggestionsService.prototype.getResults = function (term) {
        return this._http.get('http://magento.amalinze.com/api/search.php?term=' + term);
    };
    SearchSuggestionsService.prototype._handleError = function (error) {
        console.error(error.json());
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(error.json() || 'Server error');
    };
    SearchSuggestionsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], SearchSuggestionsService);
    return SearchSuggestionsService;
}());



/***/ }),

/***/ "./src/app/shared/layout/sidebar/sidebar.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/shared/layout/sidebar/sidebar.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Sidebar Holder -->\n            <nav id=\"sidebar\">\n                <div id=\"dismiss\">\n                    <i class=\"fa fa-chevron-left\"></i>\n                </div>\n\n                <div class=\"sidebar-header\">\n                    <h5>Shop by Category</h5>\n                </div>\n\n                <ul class=\"list-unstyled components\">\n                    <li class=\"active\">\n                        <a href=\"#homeSubmenu\" data-toggle=\"collapse\" aria-expanded=\"false\"><i class=\"fa fa-mobile\"></i> Mobile Phones</a>\n                        <ul class=\"collapse list-unstyled\" id=\"homeSubmenu\">\n                            <li><a href=\"#\">Samsung</a></li>\n                            <li><a href=\"#\">Apple</a></li>\n                            <li><a href=\"#\">Infinix</a></li>\n                            <li><a href=\"#\">Samsung</a></li>\n                            <li><a href=\"#\">Apple</a></li>\n                            <li><a href=\"#\">Infinix</a></li>\n                        </ul>\n                    </li>\n                    <li>\n                        <a href=\"#\"><i class=\"fa fa-laptop\"></i> Fashion</a>\n                        <a href=\"#pageSubmenu\" data-toggle=\"collapse\" aria-expanded=\"false\"><i class=\"fa fa-laptop\"></i> T-shirts</a>\n                        <ul class=\"collapse list-unstyled\" id=\"pageSubmenu\">\n                            <li><a href=\"#\">Page 1</a></li>\n                            <li><a href=\"#\">Page 2</a></li>\n                            <li><a href=\"#\">Page 3</a></li>\n                        </ul>\n                    </li>\n                    <li>\n                        <a href=\"#\"><i class=\"fa fa-mobile\"></i> Perfumes</a>\n                    </li>\n                    <li>\n                        <a href=\"#\"><i class=\"fa fa-laptop\"></i> Laptops</a>\n                    </li>\n                </ul>\n\n            </nav>"

/***/ }),

/***/ "./src/app/shared/layout/sidebar/sidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidebarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SidebarComponent = (function () {
    function SidebarComponent() {
    }
    SidebarComponent.prototype.ngOnInit = function () {
        //mCustomScrollbar
        jQuery("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });
        jQuery('#dismiss, .overlay').on('click', function () {
            jQuery('#sidebar').removeClass('active');
            jQuery('.overlay').fadeOut();
        });
        jQuery('#sidebarCollapse').on('click', function () {
            jQuery('#sidebar').addClass('active');
            jQuery('.overlay').fadeIn();
            jQuery('.collapse.in').toggleClass('in');
            jQuery('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        //mCustomScrollbar
        jQuery("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });
        jQuery('#dismiss, .overlay').on('click', function () {
            jQuery('#sidebar').removeClass('active');
            jQuery('.overlay').fadeOut();
        });
        jQuery('#sidebarCollapse').on('click', function () {
            jQuery('#sidebar').addClass('active');
            jQuery('.overlay').fadeIn();
            jQuery('.collapse.in').toggleClass('in');
            jQuery('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    };
    SidebarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-sidebar',
            template: __webpack_require__("./src/app/shared/layout/sidebar/sidebar.component.html"),
            styles: [__webpack_require__("./src/app/shared/layout/sidebar/sidebar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/shared/layout/signup/signup.component.css":
/***/ (function(module, exports) {

module.exports = "/*---------------------------------------------\r\n Services-Homepage\r\n------------------------------------------------*/\r\n.recom_services h4{\r\n  margin: 20px 20px 20px 0px;\r\n  font-weight: 500;\r\n}\r\n.recom_services img {\r\n  height: 190px;\r\n  width: 300px;\r\n  border-radius: 5px;\r\n}\r\n.recom_services p{\r\n  margin: 10px;\r\n  color: #343434; \r\n}"

/***/ }),

/***/ "./src/app/shared/layout/signup/signup.component.html":
/***/ (function(module, exports) {

module.exports = "\n        <!-- Sign Up Modal -->\n    <div class=\"modal fade\" id=\"amalinzeModalSignup\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\" aria-hidden=\"true\">\n      <div class=\"modal-dialog \" role=\"document\">\n        <div class=\"modal-content ml-con\">\n          \n          <div class=\"modal-body md-rem\">\n            <!-- Sign up -->\n            \n            \n            <div class=\"container\" id=\"signup_anchor\">\n              <div class=\"row\">\n                <div class=\"col-8 offset-2\"  >\n                  <a href=\"/\"><div id=\"logo_img\"></div></a>\n                <div class=\"row\">\n               <div class=\"col-4 \" >\n                  <div id=\"side_img\">\n                    <img src=\"assets/images/side.png\" class=\"img-fluid\">\n                  </div>\n                  <section id=\"sign_up_text\">\n                    <h4>Sign Up</h4>\n                    \n                    <p>We do not share personal details with anyone.</p>\n                  </section>\n                  \n                </div> \n                <div class=\"col-7  \" id=\"sign_up\">  \n             \n                  <form name=\"signupForm\" #signupForm=\"ngForm\" id=\"signupForm\" (ngSubmit)=\"onSubmit(signupForm)\"  >\n                    <div class=\"form-group\">\n                      <label for=\"firstname\" >First Name</label>\n                      <input type=\"firstname\"\n                       class=\"form-control\"\n                        name=\"firstname\" \n                        id=\"firstname\" \n                       \n                         ngModel\n                         required\n                         #firstname=\"ngModel\">\n                                           \n                    <span class=\"help-block\" *ngIf=\"!firstname.valid && firstname.touched\">Please enter a valid First Name!</span>\n                    </div>\n                                        <div class=\"form-group\">\n                      <label for=\"lastname\" >Last Name</label>\n                      <input type=\"lastname\"\n                       class=\"form-control\"\n                        id=\"lastname\"\n                         name=\"lastname\" \n                           ngModel\n                           required\n                             #lastname=\"ngModel\">\n                                          <span class=\"help-block\" *ngIf=\"!lastname.valid && lastname.touched\">Please enter a valid Last Name!</span>\n\n                    </div>\n                    <div class=\"form-group\">\n                      <label for=\"email\" >Email address</label>\n                      <input type=\"email\"\n                       id=\"email\"\n                        name=\"email\" \n                        class=\"form-control\" \n                         aria-describedby=\"emailHelp\" \n                         ngModel\n                         required\n                           email\n                          #email=\"ngModel\">\n                          <span class=\"help-block\" *ngIf=\"!email.valid && email.touched\">Please enter a valid Email!</span>\n                      \n                    </div>\n                    <div class=\"form-group\">\n                      <label for=\"password\" >Password</label>\n                      <input \n                      type=\"Password\"\n                       id=\"password\" \n                       name=\"password\" \n                       class=\"form-control\" \n                       ngModel\n                       required\n                      #password=\"ngModel\">\n                          <span class=\"help-block\" *ngIf=\"!password.valid && password.touched\">Please enter a valid Password!</span>\n                    </div>\n                     <div class=\"form-group\">\n                      <label for=\"confirm\" >Confirm Password</label>\n                      <input \n                      name=\"confirm\"\n                      type=\"Password\"\n                       id=\"confirm\" \n                        class=\"form-control\" \n                        ngModel\n                        required\n                       #confirm=\"ngModel\">\n                          <span class=\"help-block\" *ngIf=\"confirm.touched && confirm.value!=password.value\">Password and Confirm Password must be same!</span>\n                    </div>\n                     <div id=\"sign_up_btn\">\n                    <button class=\"btn btn-primary blue_sign\" type=\"submit\"  [disabled]=\"!signupForm.valid\">Sign up</button>\n                    <p>Already have an account? <a id=\"show_login\" href=\"/login\">Login</a></p>\n                  </div>\n                  </form>\n                 \n                  \n                  \n             \n              </div>\n            </div>\n          \n            </div>\n            </div>\n          \n            \n          </div>\n          \n        </div>\n      </div>\n    </div>"

/***/ }),

/***/ "./src/app/shared/layout/signup/signup.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_customer__ = __webpack_require__("./src/app/models/customer.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SignupComponent = (function () {
    function SignupComponent() {
        this.model = new __WEBPACK_IMPORTED_MODULE_1__models_customer__["a" /* Customer */]('', '', '', '', '');
        this.submitted = false;
    }
    SignupComponent.prototype.onSubmit = function (form) {
        this.submitted = true;
        console.log("submitted");
    };
    SignupComponent.prototype.ngOnInit = function () {
    };
    SignupComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-signup',
            template: __webpack_require__("./src/app/shared/layout/signup/signup.component.html"),
            styles: [__webpack_require__("./src/app/shared/layout/signup/signup.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SharedModule = (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* RouterModule */]
            ],
            declarations: [],
            exports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* RouterModule */]
            ]
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/app/signuppage/signup.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/_esm5/Rx.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SignupService = (function () {
    function SignupService(http) {
        this.http = http;
        this.isLoggedIn = false;
    }
    SignupService.prototype.addCustomer = function (customer) {
        var body = JSON.stringify({
            "email": customer.email,
            "firstname": customer.firstname,
            "lastname": customer.lastname,
            "password": customer.password
        });
        return this.http.post('http://magento.amalinze.com/api/addCustomer.php', body).map(function (response) {
            var data = response.json();
            return data;
        }).catch(function (error) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Something went wrong');
        });
    };
    SignupService.prototype._handleError = function (error) {
        console.error(error.json());
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(error.json() || 'Server error');
    };
    SignupService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], SignupService);
    return SignupService;
}());



/***/ }),

/***/ "./src/app/signuppage/signuppage.component.css":
/***/ (function(module, exports) {

module.exports = "#content*{\n    width: 100%;\n    padding: 0px;\n    min-height: 100vh;\n    -webkit-transition: all 0.3s;\n    transition: all 0.3s;\n    position: absolute;\n    top: 0;\n    right: 0;\n}\n#side_img_up {\n  margin-left: -20px;\n  width: 100%;\n  height: 120vh;\n  background: url('side.dc844fd8683d2344884d.png') no-repeat;\n  background-size: cover;\n  background-position: center;\n}\n#sign_up_btn {\n  text-align: center;\n  padding-top: 15px;\n}\n#sign_up_btn a {\n\tcolor: #ff7473;\n}\n#sign_up_btn p {\n   padding-top: 10px;\n   font-size: 12px;\n}\n#sign_up_text{\n  padding-top: 150px;\n  color: #fff;\n}\n#sign_up_text h4 {\n  font-weight: bold;\n  margin-bottom: 14px;\n  padding-left: 10px;\n  text-align: left;\n  color: #fff;\n}\n#sign_up_text p {\n  color: #fff;\n  padding-left: 10px;\n  font-size: 16px;\n}\n#sign_up_text_mobile{\n  /*padding-top: 150px;*/\n  color: #343434;\n}\n#sign_up_text_mobile h4 {\n  font-weight: bold;\n  margin-bottom: 10px;\n  text-align: left;\n  color: #343434;\n}\n#sign_up_text_mobile p {\n  color: #343434;\n  font-size: 10px;\n}\n#logo_img{\n  width: 280px;\n  height: 60px;\n  margin-bottom: 50px;\n  background: url('amalinze_logo_sm.4a2fe8b2be6fe491a3a0.png') no-repeat center center / contain scroll rgb(255, 255, 255); \n}\n.form_wrapper {\n\tpadding-top: 40px;\n    padding-right: 20px;\n    padding-left: 20px;\n    margin: 0 auto;\n    width: 75vh;\n}\n.blue_sign{\n  background: #18b8e5;\n  color: #fff;\n  -ms-flex-line-pack: center;\n      align-content: center;\n  width: 100%;\n  padding-top: 11px;\n  padding-bottom: 11px;\n  border: none;\n}\n.blue_sign:hover{\n  background: #18b8e7;\n  border: none;\n}"

/***/ }),

/***/ "./src/app/signuppage/signuppage.component.html":
/***/ (function(module, exports) {

module.exports = "             \n            <!-- Sign up -->\n            <div class=\"container-fluid\" id=\"signup_anchor\">\n              <div class=\"row\">\n                <div class=\"col-sm-4 no_padding\" >\n                  <div id=\"side_img_up\"  class=\"hidden-sm-down\">\n                    <section id=\"sign_up_text\">\n                      <h4>Sign Up</h4>\n                      <p>We do not share your personal details with anyone.</p>\n                  </section>\n                  </div>  \n                </div>\n\n                <div class=\"col-md-8  \" id=\"sign_up\">  \n                 <div class=\"form_wrapper\">\n                   <a href=\"/\">\n                     <div id=\"logo_img\"></div>\n                   </a>\n                   <section id=\"sign_up_text_mobile\" class=\"hidden-md-up\">\n                    <h4>Sign Up</h4>\n                      <p>We do not share your personal details with anyone.</p>\n                    </section>\n                    <div  *ngIf=\"canLoad\"><img src=\"assets/images/loader.gif\"/></div>\n\n                    <div class=\"alert alert-danger\" *ngIf=\"isSignupError\">{{signup_error}}</div>\n                     <form name=\"signupForm\" #signupForm=\"ngForm\" id=\"signupForm\" (ngSubmit)=\"onSubmit()\"  >\n                    <div class=\"form-group\">\n                      <label for=\"firstname\" >First Name</label>\n                      <input type=\"firstname\"\n                       class=\"form-control\"\n                        name=\"firstname\" \n                        id=\"firstname\" \n                       \n                         ngModel\n                         required\n                         #firstname=\"ngModel\">\n                                           \n                    <span class=\"help-block\" *ngIf=\"!firstname.valid && firstname.touched\">Please enter a valid First Name!</span>\n                    </div>\n                                        <div class=\"form-group\">\n                      <label for=\"lastname\" >Last Name</label>\n                      <input type=\"lastname\"\n                       class=\"form-control\"\n                        id=\"lastname\"\n                         name=\"lastname\" \n                           ngModel\n                           required\n                             #lastname=\"ngModel\">\n                                          <span class=\"help-block\" *ngIf=\"!lastname.valid && lastname.touched\">Please enter a valid Last Name!</span>\n\n                    </div>\n                    <div class=\"form-group\">\n                      <label for=\"email\" >Email address</label>\n                      <input type=\"email\"\n                       id=\"email\"\n                        name=\"email\" \n                        class=\"form-control\" \n                         aria-describedby=\"emailHelp\" \n                         ngModel\n                         required\n                           email\n                          #email=\"ngModel\">\n                          <span class=\"help-block\" *ngIf=\"!email.valid && email.touched\">Please enter a valid Email!</span>\n                      \n                    </div>\n                    <div class=\"form-group\">\n                      <label for=\"password\" >Password</label>\n                      <input \n                      type=\"Password\"\n                       id=\"password\" \n                       name=\"password\" \n                       class=\"form-control\" \n                       ngModel\n                       required\n                      #password=\"ngModel\">\n                          <span class=\"help-block\" *ngIf=\"!password.valid && password.touched\">Please enter a valid Password!</span>\n                    </div>\n                     <div class=\"form-group\">\n                      <label for=\"confirm\" >Confirm Password</label>\n                      <input \n                      name=\"confirm\"\n                      type=\"Password\"\n                       id=\"confirm\" \n                        class=\"form-control\" \n                        ngModel\n                        required\n                       #confirm=\"ngModel\">\n                          <span class=\"help-block\" *ngIf=\"confirm.touched && confirm.value!=password.value\">Password and Confirm Password must be same!</span>\n                    </div>\n                     <div id=\"sign_up_btn\">\n                    <button class=\"btn btn-primary blue_sign\" type=\"submit\"  [disabled]=\"!signupForm.valid\">Sign up</button>\n                    <p>Already have an account? <a id=\"show_login\" href=\"/login\">Login</a></p>\n                  </div>\n                  </form>\n\n                  \n                  \n                </div>\n                </div>\n              </div>\n            </div>\n       "

/***/ }),

/***/ "./src/app/signuppage/signuppage.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignuppageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_service__ = __webpack_require__("./src/app/signuppage/signup.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_customer__ = __webpack_require__("./src/app/models/customer.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loginpage_auth_service__ = __webpack_require__("./src/app/loginpage/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SignuppageComponent = (function () {
    function SignuppageComponent(signupService, router, authService, location) {
        this.signupService = signupService;
        this.router = router;
        this.authService = authService;
        this.location = location;
        this.canLoad = false;
        this.isSignupError = false;
        this.signup_error = "";
    }
    SignuppageComponent.prototype.onSubmit = function () {
        var _this = this;
        this.resetErrors();
        this.customer = new __WEBPACK_IMPORTED_MODULE_3__models_customer__["a" /* Customer */](this.form.value.firstname, this.form.value.lastname, this.form.value.email, this.form.value.password, '');
        this.canLoad = true;
        this.signupService.addCustomer(this.customer).subscribe(function (response) {
            _this.canLoad = false;
            if (response.status_code == 200) {
                localStorage.setItem('email', _this.form.value.email);
                localStorage.setItem('firstname', _this.form.value.firstname);
                localStorage.setItem('customer_id', response.id);
                _this.authService.isLoginSubject.next(true);
                _this.location.back();
            }
            else {
                _this.displayErrors(response.message);
            }
        }, function (error) {
            _this.canLoad = false;
            _this.displayErrors("Oops, something went wrong.Try again later");
        });
    };
    ;
    SignuppageComponent.prototype.displayErrors = function (error) {
        this.isSignupError = true;
        this.signup_error = error;
    };
    SignuppageComponent.prototype.resetErrors = function () {
        this.isSignupError = false;
        this.signup_error = "";
    };
    SignuppageComponent.prototype.ngOnInit = function () {
        if (this.authService.ifLoggedIn())
            location.href = '/';
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('signupForm'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* NgForm */])
    ], SignuppageComponent.prototype, "form", void 0);
    SignuppageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-signuppage',
            template: __webpack_require__("./src/app/signuppage/signuppage.component.html"),
            styles: [__webpack_require__("./src/app/signuppage/signuppage.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__signup_service__["a" /* SignupService */], __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_5__loginpage_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_6__angular_common__["f" /* Location */]])
    ], SignuppageComponent);
    return SignuppageComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
var environment = {
    production: false,
    api_url: 'https://conduit.productionready.io/api'
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* enableProdMode */])();
}
var bootstrapPromise = Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
// Logging bootstrap information
bootstrapPromise.then(function (success) { return console.log("Bootstrap success"); })
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map