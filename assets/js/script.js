        // Main Javascript -- 

        (function(){
               //IIFE for all JS custom functionalities

               //mCustomScrollbar
               $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#dismiss, .overlay').on('click', function () {
                    $('#sidebar').removeClass('active');
                    $('.overlay').fadeOut();
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').addClass('active');
                    $('.overlay').fadeIn();
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });

                //Thumbnail Slider
               $('.owl-carousel').owlCarousel({
                    rtl:true,
                    loop:true,
                    margin:10,
                    nav:true,
                    autoplay:false,
                    dots: false,
                    autoplayHoverPause: true,
                    responsive:{
                        0:{
                            items:1
                        },
                        600:{
                            items:3
                        },
                        800:{
                            items:4
                        },
                        1000:{
                            items:4
                        }
                    }
                });

               //Sign up transitions

               //default state of the sign up page -- hide it
               // $("#signup_anchor").hide();
                
               //show sign up
               /*jQuery("#amalinzeModalSignUpBtn").click(function(){
                 alert("Ok");
                  $("#login_anchor").hide();

                  $("#signup_anchor").show();
                  $("#amalinzeModalSignUp").modal("show");

                
               });

               //show login
               $("#amalinzeLoginBtn").click(function(){
                 
                  $("#signup_anchor").hide();

                  $("#login_anchor").show();
                
               });
           */
            })();

        